import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";

const TextBorder = (props) => (
    <View style={[styles.container]}>
            <Text style={[styles.text,props.style]}>{props.text}</Text>
        <View style={styles.border}/>
    </View>
);
TextBorder.propTypes = {
    style: PropTypes.object,
    textStyle: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    // margin:PropTypes.string,

};
TextBorder.defaultProps = {
    style: {},
    textStyle:{},
    text:'',
    touch:function () {},

    // margin:''
};
export default TextBorder;
