import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        // marginVertical:5,
    },
    text:{
        color:colors.black,
        fontSize:20,
        fontFamily:'Raleway-SemiBold',
        // alignSelf:'center'
    },
    border:{
        width:38,
        borderColor: colors.brandColor,
        borderWidth:1
    }

})

export default styles
