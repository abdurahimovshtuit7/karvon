import {StyleSheet} from 'react-native';
import colors from "assets/styles/colors";

export default StyleSheet.create({
    root: {padding: 20},
    title: {textAlign: 'center', fontSize: 30},
    codeFieldRoot: {
        marginTop: 20,
        width: '60%',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    cellRoot: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#cccccc',
        borderBottomWidth: 1,
    },
    cellText: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
    },
    focusCell: {
        borderBottomColor: colors.brandColor,
        borderBottomWidth: 2,
    },
});
