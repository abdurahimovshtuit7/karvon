import React, {Component} from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ImageBackground, Dimensions,
} from 'react-native';
import styles from './styles';
import Components from '../index';
import Heart from 'assets/images/SVG/Heart'
import Minus from 'assets/images/SVG/Minus'
// import SaveIcon from '../../assets/images/SVG/Save'


const windowWidth = Dimensions.get('window').width;

class Poster extends Component {
    state = {
        isActive: false,
    };

    render() {
        const {index, uri,item, saved, savedId, list, width, share, touch} = this.props;
        const Uri =  uri ? uri :require('assets/images/Intersection1.png')

        // const exampleImageUri = Image.resolveAssetSource(item.avatar).uri
        return (

            <TouchableOpacity style={[styles.boxStyle, {width: width}]}  onPress={() => {
                touch()
            }}>
                <ImageBackground
                    style={[styles.image, {backgroundColor: `rgba(0, 0, 0, 0.25)`}]}
                    resizeMode={'cover'}
                    // resizeMethod={"scale"} // <-------  this helped a lot as OP said
                    progressiveRenderingEnabled={true} //---- as well as this
                    source={Uri}
                >
                    <View style={[styles.textView]}>
                        <Text style={styles.text1}>

                        </Text>
                    </View>
                </ImageBackground>
                <View style={styles.rows}>
                    <Components.UserImage
                        style={styles.icon}
                        // uri={ require('../../assets/images/Ellipse.png')}
                        username={'Aleksey Petrov'}
                        textStyle={styles.iconText}
                    />
                    <View style={styles.view}>
                        <Text style={styles.name} numberOfLines={1}>
                            Aleksey Petrov
                        </Text>
                    </View>
                </View>
                <Text style={styles.description}>Excepteur sint occaecat cupidatat non proident, sunt
                    in culpa qui officia deserunt mollit anim.</Text>
                <View style={styles.commentMain}>
                <View style={styles.comment}>
                    <View style={styles.addRow}>
                        <Heart/>
                        <Text style={styles.number}>
                            609
                        </Text>

                    </View>
                    <View style={styles.addRow}>
                        <Minus/>
                        <Text style={styles.number}>
                            120
                        </Text>
                    </View>
                </View>
                    <Text style={styles.share}>
                        {share}
                    </Text>
                </View>
            </TouchableOpacity>

        );
    }
}

export default Poster;
