import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // marginHorizontal:8,
        // marginVertical:10,
        // width:'100%',
        // borderWidth:1,
        // justifyContent:'center',
    },
    boxStyle: {
        // flex:0.48,
        // flexGrow:1,
        // alignSelf: 'stretch',
        // flexWrap:'wrap',
        // borderWidth:1,
        // marginHorizontal:5,
        marginVertical:10,
        borderRadius:10,
        width: '48%',


    },
    text:{
         paddingHorizontal:10,
        // paddingTop:8,
         fontSize:18,
         color:colors.primary,
        fontWeight:'600',
        paddingVertical:10
    } ,
    about:{
        paddingHorizontal:10,
        // paddingTop:8,
        fontSize:12,
        color:colors.primary,
        fontWeight:'500',
        // paddingVertical:10
    } ,
    image:{
        overflow:'hidden',
        borderRadius:5,
        width: '100%',
        // width:"100%",
        height:173,
        // borderWidth:1,
        // paddingHorizontal:44,
        // paddingVertical:30,
        // justifyContent:'center',
        // alignItems:'center',
    },
    place:{
        paddingHorizontal:10,
        // paddingTop:8,
        fontSize:12,
        color:colors.second,
        fontWeight:'500',
        paddingTop:5,
        paddingBottom:8,
    },
    box:{
        position: 'absolute',
        right:7,
        top:7,
        borderRadius:5,
        overflow: 'hidden',
        // borderWidth:1,
        paddingHorizontal:5,
        paddingVertical: 4,
        // backgroundColor:'#fff'
    },
    textView:{
        backgroundColor:`rgba(0, 0, 0, 0.3)`,
        position:'absolute',
        borderRadius:7,
        top:122,
        left:8
    },
    rows:{
        // marginLeft:10,
        flexDirection:'row',
        // borderWidth:1,
        alignItems: 'center',
        // width:'68%',
        paddingTop:16,
        paddingBottom: 3
    },
    icon:{
        // position: "absolute",
        // right: 0,
        // top:0,
        // borderWidth:3,
        width:36,
        height:36,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20,
        borderColor:'white',
        // paddingLeft:10

    },
    iconText:{
        fontSize:12
    },
    view:{
        paddingLeft: 7,
    },
    name:{
        fontSize: 14,
        color:colors.primary,
        fontFamily:'Poppins-Bold'
    },
    comment:{
        flexDirection: 'row',
    },
    addRow:{
        flexDirection: 'row',
        paddingRight: 28
    },
    number:{
        paddingLeft:8,

    },
    description:{
        fontSize:12,
        fontFamily: 'Poppins-Regular',
        color:colors.lightGray
    },
    commentMain:{
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingVertical:13,

    },
    share:{
        color:colors.brandColor,
        fontFamily:'Poppins-Bold',
        fontSize:14,
        textTransform: 'uppercase'
    }

})
export default styles
