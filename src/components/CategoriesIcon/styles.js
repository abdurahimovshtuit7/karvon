import {Dimensions, StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
const {height, width} = Dimensions.get('window');


const styles = StyleSheet.create({
    container:{
        marginVertical:5,
        marginHorizontal:1,
    },
    button:{
        // marginHorizontal:"9%",
        paddingVertical:5,
        width:0.91*width/5,
        // borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        // borderRadius:40,
        // marginBottom:"5%",
        // marginTop:'8%',
        // elevation:30
    },
    text:{
        color:colors.white,
        paddingTop:5,
        fontSize:12,
        fontFamily:'Raleway-Regular',
        // alignSelf:'center',
        textAlign:'center',
        paddingHorizontal:2,
        // textTransform: 'uppercase'

    }

})

export default styles
