import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import Home from 'assets/images/SVG/Tabs/Home'
import colors from "assets/styles/colors";
import Gift from '../../assets/images/SVG/Icons/Gift';

const Submit = (props) => (
    <View style={[styles.container,props.style]}>
        <TouchableOpacity style={[styles.button,{backgroundColor: props.background}]} onPress={props.touch}>
            {/*{props.icon}*/}
            {/*<Home color={colors.brandColor} size={25}/>*/}
            <Text style={[styles.text,{color:props.color}]}>{props.text}</Text>
        </TouchableOpacity>
    </View>
);
Submit.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    background:PropTypes.string,
    color:PropTypes.string,
    icon:PropTypes.any
    // margin:PropTypes.string,

};
Submit.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
    background:'',
    color:'',
    icon:<Gift size={42}/>
    // margin:''
};
export default Submit;
