import {Dimensions, StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors';
var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container:{
        // paddingHorizontal: 17,
        // borderRadius:14,
        marginTop:10,
        // height:'60%'
        // position:'absolute',

        // borderWidth: 1,


    },
    wrapper: {
        // backgroundColor:'green',
        height:150,
        paddingHorizontal:14,
        // paddingVertical:50,
        // width:'100%',
        // borderRadius:3,
        // borderWidth: 1,
        // borderRadius:10


    },
    slide1: {
        // flex: 1,
        // width:'100%',
        flexWrap:'wrap',
        flexGrow:1,
        flexDirection:'row',
        // paddingVertical:50,
        // justifyContent:'space-between'

    },
    slide2: {
        flexWrap:'wrap',
        flexGrow:1,
        flexDirection:'row',
        // flex: 1,

    },

    dotStyle:{
        backgroundColor: 'rgba(0,0,0,.7)',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
        // borderColor:colors.lightGray,
        // borderWidth:1
    },
    activeDotStyle:{
        backgroundColor: colors.lightGray,
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
    },
    Pagination:{
        bottom:10
    },
    images:{
        width:0.93*width,
        height: 35*height/100,
        borderRadius: 15,
        overflow: "hidden",
    },
})

export default styles
