import React, { Component } from 'react';
import {
    ImageBackground,
    Text,
    View,
} from 'react-native';

import Swiper from 'react-native-swiper';
import Components from '../index'

//----------SVG------
import Free from 'assets/images/SVG/Icons/Free'
import Gift from 'assets/images/SVG/Icons/Gift'
import MapPointer from 'assets/images/SVG/Icons/MapPointer'
import Tshirt from 'assets/images/SVG/Icons/Tshirt'
import Supermarket from 'assets/images/SVG/Icons/Supermarket'
import Diamond from 'assets/images/SVG/Icons/Diamond'
import Monitor from 'assets/images/SVG/Icons/Monitor'
import Basket from 'assets/images/SVG/Icons/Basket'
import Flame from 'assets/images/SVG/Icons/Flame'
import Friday from 'assets/images/SVG/Icons/Friday'


import styles from "./styles";
import PropTypes from 'prop-types';
// import Screen from '../Layout';

const SwImage = (props) => {
    return (
        <View style={styles.container}>
            <Swiper style={styles.wrapper}
                    dot={<View style={styles.dotStyle} />}
                    activeDot={<View style={styles.activeDotStyle} />}
                    paginationStyle={styles.Pagination}
                    loop={true}
                    // autoplay={true}
                    // autoplayTimeout={2.5}

            >
                <View style={styles.slide1}>
                    <Components.CategoriesIcon text={'Карвон Глобал'} icon={<MapPointer size={42}/>}/>
                    <Components.CategoriesIcon text={'Топ Фашион'} icon={<Diamond size={42}/>}/>
                    <Components.CategoriesIcon text={'Цифровые товари'} icon={<Monitor size={42}/>}/>
                    <Components.CategoriesIcon text={'Лучший падарок'} icon={<Gift size={42}/>}/>
                    <Components.CategoriesIcon text={'Регулярная доставка'} icon={<Free size={45}/>}/>
                    <Components.CategoriesIcon text={'Супермаркет'} icon={<Basket size={42}/>}/>
                    <Components.CategoriesIcon text={'Дом и сад'} icon={<Supermarket size={42}/>}/>
                    <Components.CategoriesIcon text={'Одежда и обувь'} icon={<Tshirt size={42}/>}/>
                    <Components.CategoriesIcon text={'Черная пятница'} icon={<Friday size={42}/>}/>
                    <Components.CategoriesIcon text={'Акции'} icon={<Flame size={42}/>}/>
                </View>
                <View style={styles.slide2}>
                    <Components.CategoriesIcon text={'Регулярная доставка'} icon={<Free size={45}/>}/>
                    <Components.CategoriesIcon text={'Супермаркет'} icon={<Basket size={42}/>}/>
                    <Components.CategoriesIcon text={'Дом и сад'} icon={<Supermarket size={42}/>}/>
                    <Components.CategoriesIcon text={'Одежда и обувь'} icon={<Tshirt size={42}/>}/>
                    <Components.CategoriesIcon text={'Акции'} icon={<Flame size={42}/>}/>
                    <Components.CategoriesIcon text={'Цифровые товари'} icon={<Monitor size={42}/>}/>
                    <Components.CategoriesIcon text={'Лучший падарок'} icon={<Gift size={42}/>}/>

                </View>

            </Swiper>
        </View>
    );

}
SwImage.propTypes = {
    style: PropTypes.object
};
SwImage.defaultProps = {
    style: {}
};
export default SwImage;
