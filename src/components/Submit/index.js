import React from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";

const Submit = (props) => (
    <View style={[styles.container,props.style]}>
        <TouchableOpacity style={[styles.button,{backgroundColor: props.background}]} onPress={props.touch}>
            <Text style={[styles.text,{color:props.color}]}>{props.text}</Text>
        </TouchableOpacity>
    </View>
);
Submit.propTypes = {
    style: PropTypes.object,
    text:PropTypes.string,
    touch:PropTypes.func,
    background:PropTypes.string,
    color:PropTypes.string,
    // margin:PropTypes.string,

};
Submit.defaultProps = {
    style: {},
    text:'',
    touch:function () {},
    background:'',
    color:'',
    // margin:''
};
export default Submit;
