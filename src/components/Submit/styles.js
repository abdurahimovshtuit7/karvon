import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{

    },
    button:{
        marginHorizontal:"9%",
        paddingVertical:13,
        justifyContent:'center',
        borderRadius:40,
        marginBottom:"5%",
        // marginTop:'8%',
        // elevation:30
    },
    text:{
        color:colors.white,
        fontSize:16,
        fontFamily:'Raleway-Bold',
        alignSelf:'center',
        textTransform: 'uppercase'

    }

})

export default styles
