// import React from 'react';
// import {
//     TextInput,
//     View,
//     Text
// } from 'react-native';
// import PropTypes from 'prop-types';
//
// import {
//     TextField,
//     FilledTextField,
//     OutlinedTextField,
// } from 'react-native-material-textfield';
//
// const textField = (props) => {
//     const {
//         field,
//         form: { errors, touched, handleBlur, handleChange },
//     } = props;
//     let error = props.form && touched[field.name] && errors[field.name];
//
//     const textFieldProps = {
//         error,
//         tintColor: "#787878",
//         contentInset: {
//             top: 4,
//             // label: 4,
//             // input: 8,
//             // left: 0,
//             // right: 0,
//         },
//         ...props,
//         value: field.value,
//         onChangeText: handleChange(field.name),
//         onBlur: handleBlur(field.name)
//     };
//     switch (props.type){
//         case "textField":
//             return (
//                 <TextField
//                     {...textFieldProps}
//                 />
//             );
//         case "filledTextField":
//             return (
//                 <FilledTextField
//                     {...textFieldProps}
//                 />
//             );
//         case "outlinedTextField":
//             return (
//                 <OutlinedTextField
//                     {...textFieldProps}
//                 />
//             );
//         default:
//             return null
//     }
// };
// textField.propTypes = {
//     style: PropTypes.object,
//     type: PropTypes.oneOf([ 'textField', 'filledTextField', 'outlinedTextField']),
// };
// textField.defaultProps = {
//     style: {},
//     type: "textField"
// };
// export default textField;
//
// //style: Text.propType
