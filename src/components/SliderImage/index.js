import React, { Component } from 'react';
import {
    ImageBackground,
    Text,
    View,
} from 'react-native';

import Swiper from 'react-native-swiper';
import SwiperImage from 'assets/images/SVG/SwiperImage'

import styles from "./styles";
import PropTypes from 'prop-types';
// import Screen from '../Layout';

const SwImage = (props) => {

        return (
            <View style={styles.container}>
            <Swiper style={styles.wrapper}
                    dot={<View style={styles.dotStyle} />}
                    activeDot={<View style={styles.activeDotStyle} />}
                    paginationStyle={styles.Pagination}
                    loop={true}
                    autoplay={true}
                    autoplayTimeout={5}
            >
                {/*<View style={styles.slide1}>*/}
                {/*   <SwiperImage/>*/}
                {/*</View>*/}
                {/*<View style={styles.slide2}>*/}
                {/*    <SwiperImage/>*/}
                {/*</View>*/}
                {/*<View style={styles.slide3}>*/}
                {/*    <SwiperImage/>*/}
                {/*</View>*/}
                <View style={styles.slide1}>
                    <ImageBackground source={require('assets/images/main.jpg')} style={styles.images}/>
                </View>
                <View style={styles.slide2}>
                    <ImageBackground source={require('assets/images/team.jpg')} style={styles.images}/>
                </View>
                <View style={styles.slide3}>
                    <ImageBackground source={require('assets/images/buy.jpg')} style={styles.images}/>
                </View>
                <View style={styles.slide3}>
                    <ImageBackground source={require('assets/images/team.jpg')} style={styles.images}/>
                </View>
            </Swiper>
            </View>
        );

}
SwImage.propTypes = {
    style: PropTypes.object
};
SwImage.defaultProps = {
    style: {}
};
export default SwImage;
