import {Dimensions, StyleSheet} from 'react-native';
var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container:{
        // paddingHorizontal: 17,
        // borderRadius:14,
        marginTop:3,
        paddingHorizontal:14,
        // position:'absolute',
        // zIndex:2
        // borderWidth: 1,


    },
    wrapper: {
        height:146,

        // borderRadius: 5,
        // overflow: "hidden",
        // width:'100%',
        // borderRadius:3,
        // borderWidth: 1,
        // borderRadius:10


    },
    slide1: {
        flex: 1,
        borderRadius: 5,
        overflow: "hidden",
        justifyContent: 'center',
        alignItems: 'center',
        // width:'100%',


    },
    slide2: {
        flex: 1,
        borderRadius: 5,
        overflow: "hidden",
        justifyContent: 'center',
        alignItems: 'center',

    },
    slide3: {
        flex: 1,
        borderRadius: 5,
        overflow: "hidden",
        justifyContent: 'center',
        alignItems: 'center',

    },
    dotStyle:{
        backgroundColor: 'rgba(255,255,255,.3)',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
        borderColor:'white',
        borderWidth:1
    },
    activeDotStyle:{
        backgroundColor: '#fff',
        width: 7,
        height: 7,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
    },
    Pagination:{
        bottom:10
    },
    images:{
        width:width,
        height: 35*height/100,
        // borderRadius: 15,
        // overflow: "hidden",
    },
})

export default styles
