import Input from './Input'
import Layout from './Layout'
import Submit from "./Submit"
import SelectPerson from './SelectPerson'
import InputText from './InputText'
import Picker from "./Picker";
import Confirmation from './Confirmation'
import Toast from './Toast'
import TextWithBorder from './TextWithBorder'
import Item from './Item';
import Poster from './Poster';
import UserImage from './UserImage'
import SliderImage from './SliderImage'
// import TextField from "./TextField";
import CategoriesList from './CategoriesList'
import CategoriesIcon from './CategoriesIcon'

export default {
    Input,
    Layout,
    Submit,
    SelectPerson,
    InputText,
    Picker,
    Confirmation,
    TextWithBorder,
    UserImage,
    SliderImage,
    CategoriesList,
    CategoriesIcon,
    Item,
    Poster,
    Toast
    // TextField
}
