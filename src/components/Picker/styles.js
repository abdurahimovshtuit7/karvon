import { StyleSheet } from 'react-native';
import colors from '../../assets/styles/colors'
import DropDownPicker from "react-native-dropdown-picker";

const styles = StyleSheet.create({
    container:{
        marginHorizontal:24,
        // backgroundColor:'rgba(255,255,255,0.61)',
        paddingVertical: 12,
        zIndex:1,
        // borderWidth:1
    },
    title:{
        fontSize:14,
        fontFamily:'Raleway-SemiBold',
        color:colors.black,
        paddingBottom:11,
        // paddingBottom:8
    },
    dropDownStyle:{
        borderWidth:0,
        // borderTopWidth:1,
        backgroundColor:colors.white,
        shadowColor: 'rgba(0,0,0,0.15)',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation: 1,
    },
    placeholderStyle:{
        color:colors.placeholder,
        fontFamily: 'Poppins-Regular'
    },
    labelStyle:{
        fontSize: 14,
        textAlign: 'left',
        color: '#000'
    },
    component:{
        backgroundColor:colors.white,
        shadowColor: 'rgba(0,0,0,0.15)',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 10,
        elevation: 1,
        borderWidth:0,
        borderRadius:3,
        paddingVertical:5,
    },
    red:{
        color: colors.red
    },



})

export default styles
