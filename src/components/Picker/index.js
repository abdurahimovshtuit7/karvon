import React, {Component, useState} from 'react'
import {
    ActivityIndicator,
    Image,
    SafeAreaView,
    View,
    Text,
    TouchableOpacity,
    TextInput
} from "react-native";
import styles from './styles'
import colors from "assets/styles/colors";
//import NavigationService from "../../navigators/NavigationService";
import PropTypes from 'prop-types'
import DropDownPicker from "react-native-dropdown-picker";
import Components from "../index";
// import TextInputMask from "react-native-text-input-mask";


const stylesFunction = (focus, error) => {
    console.log("Errorrr:", error)
    if (focus && !error) {
        return styles.focusedTextInput
    } else if (error) {
        return styles.error
    } else return styles.textInput
}
const  changeVisibility = (setState) => {
    setState(false);
}

const Picker = (props) => {
    const [isVisible, setState] = useState(false)
    const [item, setItem] = useState(null)

    return (
        <View style={[styles.container,{zIndex:props.zIndex}]}>
            <Text style={styles.title}>
                {props.title}
                <Text style={styles.red}>
                    *
                </Text>
            </Text>
            <DropDownPicker
                // zIndex={props.zIndex}
                items={props.items}
                placeholder={props.placeholder}
                style={styles.component}
                labelStyle={styles.labelStyle}
                placeholderStyle={styles.placeholderStyle}
                dropDownStyle={styles.dropDownStyle}
                defaultValue={item}
                // containerStyle={{}}
                isVisible={isVisible}
                onOpen={() => changeVisibility(setState)}
                onClose={() => setState( false)}
                onChangeItem={item => setItem(item.value)}
            />
        </View>
    )
}
Picker.propTypes = {
    style: PropTypes.object,
    containerStyle: PropTypes.object,
    title: PropTypes.string,
    zIndex:PropTypes.number,
    items:PropTypes.array,
    placeholder:PropTypes.string
};
Picker.defaultProps = {
    style: {},
    containerStyle: {},
    title: '',
    zIndex:1,
    items:[],
    placeholder:''
};
export default Picker
