import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors'


const styles = StyleSheet.create({
    textInput: {
        flexDirection:'row',
        paddingVertical:8,
        justifyContent:'center',
        fontSize: 12,
        width:'100%',
        // borderWidth: 1,
    },

    container:{
        marginHorizontal:'9%',
        backgroundColor:colors.white,
        paddingVertical: 21,
        // width:'80%',
        // borderWidth:1
    },
    title:{
     fontSize:14,
     fontFamily:'Raleway-Regular',
     color:'rgba(0,0,0,0.5)',
     // paddingBottom:8
    },
    text:{
        paddingLeft:10,
        fontSize:16,
        fontWeight:'500'
    },
    border:{
        borderWidth:0.7,
        flex:1,
        borderColor:'rgba(0,0,0,0.2)',
        // marginBottom:'13%'
    },

})

export default styles
