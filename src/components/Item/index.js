import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ImageBackground, Dimensions
} from "react-native";
import styles from './styles'
import SaveIcon from 'assets/images/SVG/Save'
// import Megapolis from '../../assets/SVG/MegaPolis'
// import Room from '../../assets/SVG/Room'
import NavigationService from "../../navigators/NavigationService";
import colors from "../../assets/styles/colors";
import LinearGradient from "react-native-linear-gradient";
// import moment from "moment";

const windowWidth = Dimensions.get('window').width;

class Item extends Component {
    state = {
        isActive: false
    }
    // componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
    //     moment.locale('ru')
    //     // this.bigItem()
    // }


    render() {
        const {index,uri, item, saved, savedId, list, width, bigItem,touch,height} = this.props
        const Uri =  uri ? uri :require('assets/images/Intersection1.png')
        // const save = savedId?savedId:null
        // let isSelected = list[item.id]
        // const exampleImageUri = Image.resolveAssetSource(item.avatar).uri
        return (

            <TouchableOpacity style={[styles.boxStyle, {width: width}]} onPress={() => {
              touch()
            }}>
                <ImageBackground
                    style={[styles.image, {backgroundColor: `rgba(0, 0, 0, 0.25)`,height: height}]}
                    resizeMode={"cover"}
                    // resizeMethod={"scale"} // <-------  this helped a lot as OP said
                    progressiveRenderingEnabled={true} //---- as well as this
                    source={Uri}
                >
                    <LinearGradient colors={['rgba(255,255,255,0)','rgba(255,255,255,0.1)','rgba(0,0,0,0.2)','rgba(0,0,0,0.8)']}
                                 style={styles.wrapper}/>
                    <TouchableOpacity style={styles.box} onPress={() => {
                        let active = this.state.isActive
                        this.setState({isActive: !active})
                        saved(item.id, item)
                    }}>
                        <SaveIcon
                            style={styles.save}
                            // color={isSelected ? colors.brandColor : colors.second}
                            // opacity={isSelected ? '1' : '0.8'}
                        />
                    </TouchableOpacity>

                    <View style={[styles.textView]}>
                        <TouchableOpacity style={[styles.service,{width: width?'48%':'24%'}]}>
                            <Text style={styles.textButton}>Services</Text>
                        </TouchableOpacity>
                        <Text style={styles.text1} numberOfLines={2}>
                            "ИТ-ГРАД" представил партнерскую программу для fld as ss физических лиц
                        </Text>
                    </View>
                </ImageBackground>



            </TouchableOpacity>

        )
    }
}

export default Item
