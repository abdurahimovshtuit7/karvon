import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // marginHorizontal:8,
        // marginVertical:10,
        // width:'100%',
        // borderWidth:1,
        // justifyContent:'center',
    },
    boxStyle: {
        // flex:0.48,
        // flexGrow:1,
        // alignSelf: 'stretch',
        // flexWrap:'wrap',
        // borderWidth:1,
        // marginHorizontal:5,
        marginVertical:5,
        borderRadius:3,
        backgroundColor:colors.white,
        borderColor:colors.border,
        shadowColor: "#000",
        // width: '48%',
    },
    text:{
         paddingHorizontal:10,
        // paddingTop:8,
         fontSize:18,
         color:colors.primary,
        fontWeight:'600',
        paddingVertical:10
    } ,
    about:{
        paddingHorizontal:10,
        // paddingTop:8,
        fontSize:12,
        color:colors.primary,
        fontWeight:'500',
        // paddingVertical:10
    } ,
    image:{
        overflow:'hidden',
        borderRadius:3,
        width: '100%',
        // width:"100%",
        height:176,
        // borderWidth:1,
        // paddingHorizontal:44,
        // paddingVertical:30,
        // justifyContent:'center',
        // alignItems:'center',
    },
    place:{
        paddingHorizontal:10,
        // paddingTop:8,
        fontSize:12,
        color:colors.second,
        fontWeight:'500',
        paddingTop:5,
        paddingBottom:8,
    },

    text1:{
        fontSize:12,
        color:'white',
        // padding:4,
        fontFamily:'Poppins-Regular',
        alignSelf:'center',
        // width: '90%'
    },
    save:{



    },
    wrapper: {
        height: "100%",
        //borderRadius:10
        // paddingBottom:10
        // backgroundColor:'rgba(255, 0, 255, 1.0)'

    },
    box:{
        position: 'absolute',
        right:7,
        top:7,
        borderRadius:5,
        overflow: 'hidden',
        // borderWidth:1,
        paddingHorizontal:5,
        paddingVertical: 4,
        // backgroundColor:'#fff'
    },
    textView:{
        // backgroundColor:`rgba(0, 0, 0, 0.3)`,
        position:'absolute',
        // borderRadius:7,
        // top:122,
        width: '96%',
        left:12,
        bottom:12,
        // right: 12,
    },
    service:{
        marginBottom: 10,
        width:'25%',
        borderRadius:10,
        backgroundColor: colors.yellow,
    },
    textButton:{
        // paddingHorizontal:2,
        paddingVertical:2,
        alignSelf: 'center'
    }

})
export default styles
