import React, {useState} from 'react';
import {
    View,
    Text,
    Animated,
    ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import colors from 'assets/styles/colors'

const getAvatar = (username) => {
    const userName = username || '';
    const name = userName.toUpperCase().split(' ');
    let avatarName = "";
    let avatarColor;
    if (name.length === 1) {
        avatarName = `${name[0].charAt(0)}`;
    }
    else if (name.length > 1) {
        avatarName = `${name[0].charAt(0)}${name[1].charAt(0)}`;
    }
    else {
        avatarName = '';
    }
    let sumChars = 0;
    for (let i = 0; i < userName.length; i += 1) {
        sumChars += userName.charCodeAt(i);
    }
    const Colors = [
        colors.carrot,
        colors.emerald,
        colors.peterRiver,
        colors.wisteria,
        colors.alizarin,
        colors.turquoise,
        colors.midnightBlue,
    ];
    avatarColor = Colors[sumChars % Colors.length];

    return { avatarColor, avatarName }
};

const ImageView = (props) => {
    const [loading, setValue] = useState(false);
    let defaultSource = props.defaultSource;
    if(!props.defaultSource)
        defaultSource = require('./defaultImage.jpg');
    let src = defaultSource;
    if(props.uri)
        // src = {uri: props.uri};        // Uri
        src = props.uri;

    let avatar = getAvatar(props.username);

    return(
        <View style={styles.wrapper}>

            {
                props.username && !props.uri ? (
                    <View
                        style={[
                            styles.avatarStyle,
                            props.style,
                            { backgroundColor: avatar.avatarColor }
                        ]}
                    >
                        <Text
                            style={[
                                styles.textStyle,
                                props.textStyle
                            ]}
                        >
                            {avatar.avatarName}
                        </Text>
                    </View>
                ) : (
                    <Animated.Image
                        style={[styles.image, props.style]}
                        source={src}
                        resizeMode={'cover'}
                        onLoadStart={() => setValue(true)}
                        onLoadEnd={() => {
                            setValue(false)
                        }}
                    />
                )
            }
            {props.children}
            {
                props.showPreloader && loading ? (
                    <View style={styles.spinner}>
                        <ActivityIndicator
                            color={props.spinnerColor}
                        />
                    </View>
                ) : null
            }
        </View>
    )
};
ImageView.propTypes = {
    uri: PropTypes.string,
    style: PropTypes.object,
    // count: PropTypes.string
};
ImageView.defaultProps = {
    uri: '',
    style: {},
    showPreloader: true,
    spinnerColor: "#008CFF",
    username: "",
    textStyle: {}
};
export default ImageView;
