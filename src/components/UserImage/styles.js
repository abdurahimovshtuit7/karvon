import { StyleSheet } from 'react-native';
import {center} from "assets/styles/commons";
import colors from 'assets/styles/colors'

export default StyleSheet.create({
    wrapper: {

    },
    image: {
        width: 80,
        height: 80,
        borderRadius: 50
    },
    avatarStyle: {
        ...center,
        width: 80,
        height: 80,
        borderRadius: 50,
    },
    spinner: {
        position: "absolute",
        left: 0,
        right: 0,
        top: '50%'
    },
    textStyle: {
        color: colors.white,
        fontSize: 24,
        backgroundColor: colors.backgroundTransparent,
        fontWeight: '700',
    }
});
