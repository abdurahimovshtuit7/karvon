import React,{useState} from 'react';
import {
    View,
    TouchableOpacity,
    Text

} from 'react-native';
import PropTypes from 'prop-types'
import styles from './styles';
import colors from "assets/styles/colors";
import Person from 'assets/images/SVG/Person'
import HandShake from 'assets/images/SVG/HandShake'

const Screen = (props) => {
   const [value, setValue] = useState(null)
    return (
        <View style={styles.view}>
            {props.PROP.map(res => {
                return (
                    <TouchableOpacity key={res.key}
                                      activeOpacity={0.9}
                                      style={[styles.container, {
                        backgroundColor: (value === res.key ? colors.brandColor : colors.white),
                        borderWidth: (value === res.key ? null : 1)
                    }]}
                                      onPress={() => {
                                          setValue(res.key);
                                          props.onSelected(res.key)
                                      }}
                    >
                        {res.key === '1'?(
                          <Person color={(value === res.key?colors.white:colors.black)}/>
                        ):(<HandShake color={(value === res.key?colors.white:colors.black)}/>)
                        }
                        <Text style={[styles.text,{color:(value === res.key?colors.white:colors.black)}]}>
                            {res.title}
                        </Text>
                    </TouchableOpacity>
                );
            })}

        </View>)
};
Screen.propTypes = {
    style: PropTypes.object,
    PROP: PropTypes.array,
    onSelected: PropTypes.func
};
Screen.defaultProps = {
    style: {},
    PROP: [],
    onSelected:function () {}
};
export default Screen;
