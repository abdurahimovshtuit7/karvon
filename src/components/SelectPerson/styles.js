import {StyleSheet, Platform} from 'react-native';
import colors from '../../assets/styles/colors'
import { Dimensions } from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;


const styles = StyleSheet.create({
    view:{
        marginTop:16,
    },
    container: {
        // marginTop:27,
        borderWidth:1,
        marginHorizontal:28,
        borderRadius: 10,
        borderColor: colors.border,
        marginVertical:12,
        paddingVertical: 40,
        justifyContent:'center',
        alignItems:'center'
    },
    text:{
        fontSize:16,
        marginTop:15,
        fontFamily:'Poppins-SemiBold',
        // marginBottom:25,
        color:colors.black,
        // width:'90%'
      paddingHorizontal:20,
        alignSelf:'center'
    },



});

export default styles
