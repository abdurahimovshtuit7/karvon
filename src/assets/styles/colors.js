const brandColor = '#4E7DF1';
const brand = '#c9d5f1';
const yellow = '#FEDB52';
const red = '#FF0000';
const white = '#FFF';
const footer = 'rgba(255, 255,255 , 0.28)'
const black = '#000'
const border ='#DFDFDF'
const placeholder = '#c4c4c6'
const gray = '#707070'
const lightGray = '#7B7B7B'
const inActive = '#848484'
const background = '#F3F3F3'

export default {
  brandColor,
  yellow,
  red,
  white,
  footer,
  black,
  brand,
  border,
  placeholder,
  gray,
  inActive,
  background,
  lightGray,
  backgroundTransparent: 'transparent',
  mainColor: "#0550ea",
  carrot: '#e67e22',
  emerald: '#2ecc71',
  peterRiver: '#3498db',
  wisteria: '#8e44ad',
  alizarin: '#e74c3c',
  turquoise: '#1abc9c',
  midnightBlue: '#2c3e50'
};
