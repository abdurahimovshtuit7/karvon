import * as React from "react";
import Svg, { Defs, ClipPath, Path, G } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={21.002} height={20.47} viewBox="0 0 21.002 20.47" {...props}>
            <Defs>
                <ClipPath id="a">
                    <Path fill="none" d="M0 0H21.002V20.47H0z" />
                </ClipPath>
            </Defs>
            <G data-name="Repeat Grid 2" clipPath="url(#a)">
                <Path
                    data-name="Icon awesome-arrow-left"
                    d="M12.07 20.864L11.03 21.9a1.12 1.12 0 01-1.589 0L.328 12.8a1.12 1.12 0 010-1.589L9.441 2.1a1.12 1.12 0 011.589 0l1.04 1.036a1.126 1.126 0 01-.019 1.608L6.4 10.125h13.475A1.122 1.122 0 0121 11.25v1.5a1.122 1.122 0 01-1.125 1.125H6.4l5.648 5.381a1.118 1.118 0 01.022 1.608z"
                    transform="translate(-14 -55) translate(14.002 53.235)"
                    fill="#fff"
                />
            </G>
        </Svg>
    );
}

export default SvgComponent;
