import * as React from "react";
import Svg, { Defs, ClipPath, Rect, G, Path } from "react-native-svg";

const SvgComponent=(props)=> {
    return (
        <Svg width={12.925} height={14.772} viewBox="0 0 12.925 14.772" {...props}>
            <Defs>
                <ClipPath id="clip-path">
                    <Rect width={12.925} height={14.772} fill="none" />
                </ClipPath>
            </Defs>
            <G
                id="Repeat_Grid_4"
                data-name="Repeat Grid 4"
                clipPath="url(#clip-path)"
            >
                <G transform="translate(-17 -331.973)">
                    <Path
                        id="Icon_awesome-user"
                        data-name="Icon awesome-user"
                        d="M6.463,7.386A3.693,3.693,0,1,0,2.77,3.693,3.693,3.693,0,0,0,6.463,7.386Zm2.585.923H8.566a5.022,5.022,0,0,1-4.207,0H3.878A3.879,3.879,0,0,0,0,12.187v1.2a1.385,1.385,0,0,0,1.385,1.385H11.541a1.385,1.385,0,0,0,1.385-1.385v-1.2A3.879,3.879,0,0,0,9.048,8.309Z"
                        transform="translate(17 331.973)"
                    />
                </G>
            </G>
        </Svg>
    );
}

export default SvgComponent;
