import * as React from "react";
import Svg, { Defs, ClipPath, Rect, G, Path, Circle } from "react-native-svg";

const SvgComponent=(props)=> {
    return (
        <Svg width={16.521} height={15.23} viewBox="0 0 16.521 15.23" {...props}>
            <Defs>
                <ClipPath id="clip-path">
                    <Rect width={16.521} height={15.23} fill="none" />
                </ClipPath>
            </Defs>
            <G id="calendar" clipPath="url(#clip-path)">
                <G id="calendar-2" data-name="calendar">
                    <G id="Group_1964" data-name="Group 1964" transform="translate(0 0)">
                        <G id="Group_1963" data-name="Group 1963">
                            <Path
                                id="Path_486"
                                data-name="Path 486"
                                d="M15.876,21.3H12.7v-.651a.645.645,0,1,0-1.291,0V21.3H5.114v-.651a.645.645,0,1,0-1.291,0V21.3H.645A.645.645,0,0,0,0,21.941V34.585a.645.645,0,0,0,.645.645h15.23a.645.645,0,0,0,.645-.645V21.941A.645.645,0,0,0,15.876,21.3ZM15.23,33.94H1.291V22.587H3.823v.651a.645.645,0,0,0,1.291,0v-.651h6.293v.651a.645.645,0,0,0,1.291,0v-.651H15.23Z"
                                transform="translate(0 -20)"
                            />
                        </G>
                    </G>
                    <G
                        id="Group_1966"
                        data-name="Group 1966"
                        transform="translate(7.389 6.065)"
                    >
                        <G id="Group_1965" data-name="Group 1965">
                            <Circle
                                id="Ellipse_22"
                                data-name="Ellipse 22"
                                cx={0.871}
                                cy={0.871}
                                r={0.871}
                            />
                        </G>
                    </G>
                    <G
                        id="Group_1968"
                        data-name="Group 1968"
                        transform="translate(7.389 9.856)"
                    >
                        <G id="Group_1967" data-name="Group 1967">
                            <Circle
                                id="Ellipse_23"
                                data-name="Ellipse 23"
                                cx={0.871}
                                cy={0.871}
                                r={0.871}
                            />
                        </G>
                    </G>
                    <G
                        id="Group_1970"
                        data-name="Group 1970"
                        transform="translate(3.598 6.065)"
                    >
                        <G id="Group_1969" data-name="Group 1969">
                            <Circle
                                id="Ellipse_24"
                                data-name="Ellipse 24"
                                cx={0.871}
                                cy={0.871}
                                r={0.871}
                            />
                        </G>
                    </G>
                    <G
                        id="Group_1972"
                        data-name="Group 1972"
                        transform="translate(11.181 6.065)"
                    >
                        <G id="Group_1971" data-name="Group 1971">
                            <Circle
                                id="Ellipse_25"
                                data-name="Ellipse 25"
                                cx={0.871}
                                cy={0.871}
                                r={0.871}
                            />
                        </G>
                    </G>
                    <G
                        id="Group_1974"
                        data-name="Group 1974"
                        transform="translate(3.598 9.856)"
                    >
                        <G id="Group_1973" data-name="Group 1973">
                            <Circle
                                id="Ellipse_26"
                                data-name="Ellipse 26"
                                cx={0.871}
                                cy={0.871}
                                r={0.871}
                            />
                        </G>
                    </G>
                    <G
                        id="Group_1976"
                        data-name="Group 1976"
                        transform="translate(11.181 9.856)"
                    >
                        <G id="Group_1975" data-name="Group 1975">
                            <Circle
                                id="Ellipse_27"
                                data-name="Ellipse 27"
                                cx={0.871}
                                cy={0.871}
                                r={0.871}
                            />
                        </G>
                    </G>
                </G>
            </G>
        </Svg>
    );
}

export default SvgComponent;
