import * as React from "react";
import Svg, { Defs, ClipPath, Rect, G } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={27} height={20} viewBox="0 0 27 20" {...props}>
            <Defs>
                <ClipPath id="clip-path">
                    <Rect width={27} height={20} fill="none" />
                </ClipPath>
            </Defs>
            <G
                id="Repeat_Grid_2"
                data-name="Repeat Grid 2"
                clipPath="url(#clip-path)"
            >
                <G
                    id="Group_9252"
                    data-name="Group 9252"
                    transform="translate(-14 -57)"
                >
                    <Rect
                        id="Rectangle_1556"
                        data-name="Rectangle 1556"
                        width={27}
                        height={4}
                        rx={2}
                        transform="translate(14 57)"
                        fill="#fff"
                    />
                    <Rect
                        id="Rectangle_1557"
                        data-name="Rectangle 1557"
                        width={17}
                        height={4}
                        rx={2}
                        transform="translate(14 65)"
                        fill="#fff"
                    />
                    <Rect
                        id="Rectangle_1558"
                        data-name="Rectangle 1558"
                        width={22}
                        height={4}
                        rx={2}
                        transform="translate(14 73)"
                        fill="#fff"
                    />
                </G>
            </G>
        </Svg>
    );
}

export default SvgComponent;
