import * as React from "react";
import Svg, { Defs, ClipPath, Path, G } from "react-native-svg";

const SvgComponent= (props) => {
    return (
        <Svg width={19.544} height={15.201} viewBox="0 0 19.544 15.201" {...props}>
            <Defs>
                <ClipPath id="a">
                    <Path fill="none" d="M0 0H19.544V15.201H0z" />
                </ClipPath>
            </Defs>
            <G data-name="Repeat Grid 5" clipPath="url(#a)">
                <Path
                    data-name="Icon awesome-comments"
                    d="M14.115 7.679c0-3-3.159-5.429-7.058-5.429S0 4.679 0 7.679A4.6 4.6 0 001.289 10.8a7.637 7.637 0 01-1.214 1.85.27.27 0 00-.051.3.266.266 0 00.248.163 5.974 5.974 0 003.01-.848 8.634 8.634 0 003.777.848c3.897-.005 7.056-2.435 7.056-5.434zm4.14 7.465a4.6 4.6 0 001.289-3.122c0-2.27-1.815-4.214-4.387-5.025a5.042 5.042 0 01.044.682c0 3.593-3.654 6.515-8.143 6.515a10.173 10.173 0 01-1.076-.064 7.335 7.335 0 006.5 3.322 8.587 8.587 0 003.777-.848 5.974 5.974 0 003.01.848.268.268 0 00.248-.163.272.272 0 00-.051-.3 7.556 7.556 0 01-1.211-1.845z"
                    transform="translate(-251.257 -331.996) translate(251.257 329.746)"
                />
            </G>
        </Svg>
    );
}

export default SvgComponent;
