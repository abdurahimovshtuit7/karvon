import * as React from "react";
import Svg, { Defs, ClipPath, Path, G } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={16} height={16} viewBox="0 0 16 16" {...props}>
            <Defs>
                <ClipPath id="a">
                    <Path fill="none" d="M0 0H16V16H0z" />
                </ClipPath>
            </Defs>
            <G data-name="Component 97 \u2013 5" clipPath="url(#a)">
                <Path
                    d="M14.746 1.3a4.3 4.3 0 00-6.119 0l-.6.6-.6-.6A4.327 4.327 0 001.3 7.423l6.721 6.721 6.721-6.721a4.3 4.3 0 000-6.119"
                    transform="translate(-.025 .975)"
                    fill="#4e7df1"
                    fillRule="evenodd"
                />
                <Path data-name="Rectangle 1450" fill="none" d="M0 0H16V16H0z" />
            </G>
        </Svg>
    );
}

export default SvgComponent;
