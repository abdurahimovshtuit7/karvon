import * as React from "react";
import Svg, { Path } from "react-native-svg";

const SvgComponent = (props)=> {
    return (
        <Svg
            width={props.size}
            height={props.size}
            viewBox="0 0 24 24"
            fill="none"
            stroke="currentColor"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
            className="feather feather-home"
            {...props}
        >
            <Path d="M3 9l9-7 9 7v11a2 2 0 01-2 2H5a2 2 0 01-2-2z" />
            <Path d="M9 22L9 12 15 12 15 22" />
        </Svg>
    );
}

export default SvgComponent;
