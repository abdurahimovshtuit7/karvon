import * as React from "react";
import Svg, { Defs, ClipPath, Rect, G, Path } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={21.75} height={21.75} viewBox="0 0 21.75 21.75" {...props}>
            <Defs>
                <ClipPath id="clip-path">
                    <Rect width={21.75} height={21.75} fill="none" />
                </ClipPath>
            </Defs>
            <G
                id="Icon_feather-search"
                data-name="Icon feather-search"
                clipPath="url(#clip-path)"
            >
                <G
                    id="Icon_feather-search-2"
                    data-name="Icon feather-search"
                    transform="translate(-3.75 -3.75)"
                >
                    <Path
                        id="Path_4388"
                        data-name="Path 4388"
                        d="M22.224,13.362A8.862,8.862,0,1,1,13.362,4.5,8.862,8.862,0,0,1,22.224,13.362Z"
                        fill="none"
                        stroke="#848484"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={1.5}
                    />
                    <Path
                        id="Path_4389"
                        data-name="Path 4389"
                        d="M29.794,29.794l-4.819-4.819"
                        transform="translate(-5.354 -5.354)"
                        fill="none"
                        stroke="#848484"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={1.5}
                    />
                </G>
            </G>
        </Svg>
    );
}

export default SvgComponent;
