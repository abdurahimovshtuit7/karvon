import * as React from "react";
import Svg, { Defs, ClipPath, Path, G } from "react-native-svg";

function SvgComponent(props) {
    return (
        <Svg width={16} height={16} viewBox="0 0 16 16" {...props}>
            <Defs>
                <ClipPath id="a">
                    <Path fill="none" d="M0 0H16V16H0z" />
                </ClipPath>
            </Defs>
            <G data-name="Repeat Grid 1" clipPath="url(#a)">
                <G
                    data-name="Component 96 \u2013 14"
                    transform="translate(0 -2) translate(0 2)"
                    clipPath="url(#a)"
                >
                    <Path
                        d="M5 8a.945.945 0 001 1h4a1 1 0 001-1 .945.945 0 00-1-1H6a1 1 0 00-1 1zM0 5a4.951 4.951 0 015-5h6a4.951 4.951 0 015 5v10a.945.945 0 01-1 1H5a4.951 4.951 0 01-5-5z"
                        fill="#4e7df1"
                        fillRule="evenodd"
                    />
                </G>
            </G>
        </Svg>
    );
}

export default SvgComponent;
