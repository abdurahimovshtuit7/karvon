

export default (api) => {
    return {
        Login: (data) => {
            return api.post(`/auth/login`, data.data)
        },
        signUp: (data) => {
            return api.post(`/auth/sign-up`,data.data)
        },
        resentPost: (data) => {
            return api.post(`/auth/resent`,data.data)
        },
        confirm: (data)=>{
            return api.post(`/auth/confirm`, data.data)
        },
        facebookConfirm:(data)=>{
            return api.post(`/auth/facebook`,data.data)
        }
    }
}
