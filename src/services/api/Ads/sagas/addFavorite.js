import { call, put, takeEvery, all } from 'redux-saga/effects'

import { addFavorite } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(addFavorite.request(request));
        const currentToken = yield call(TokenStorage.get)

        if(currentToken)
            yield call(api.setToken,currentToken)

        const response = yield call(api.ads.addFavorite, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                addFavorite.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(addFavorite.failure({e,request}))
    } finally {
        yield put(addFavorite.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(addFavorite.TRIGGER, trigger, api)
}
