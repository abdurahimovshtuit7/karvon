import { all } from "redux-saga/effects";

import getItems from './getItems'
import getCategories from './getCategories'
import addFavorite from './addFavorite'
import favoritesList from './favoritesList'

export default function * sagas (api) {
    yield all(
        [
            getItems(api),
            getCategories(api),
            addFavorite(api),
            favoritesList(api)
        ]
    )
}
