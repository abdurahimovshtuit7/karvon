import buildUrl from 'build-url'

export default (api) => {
    return {
        getItems: (data) => {
            let url = buildUrl('/home/items',{
                queryParams:{
                    page:data.page
                }
            })
            return api.get(url)
        },
        getCategories: () => {
            return api.get(`/categories`)
        },
        addFavorite: (data)=>{
            let url = buildUrl('/favorites/add/',{
                path:data.data.id
            })
            return api.post(url)
        },
        favoritesList:()=>{
            return api.get(`/favorites/list`)
        }
    }
}
