

export default (api) => {
    return {
        Login: (data) => {
            return api.post(`/api/auth/login`, data.data)
        },
        signUp: (data) => {
            return api.post(`/api/auth/register`,data.data)
        },
        forgotPassword: (data) => {
            return api.post(`/api/auth/forgotpasswordtosms`,data.data)
        },
        confirm: (data)=>{
            return api.post(`/auth/confirm`, data.data)
        },
        editPassword:(data)=>{
            return api.post(`/api/auth/editpassword`,data.data)
        }
    }
}
