import { createRoutine, promisifyRoutine } from 'redux-saga-routines'

export const Login = createRoutine('LOGIN');
export const signUp = createRoutine('SIGN_UP');
export const forgotPassword = createRoutine('FORGOT_PASSWORD');
export const confirm = createRoutine('CONFIRM')
export const editPassword = createRoutine('EDIT_PASSWORD')


export default {
    Login: promisifyRoutine(Login),
    signUp: promisifyRoutine(signUp),
    forgotPassword: promisifyRoutine(forgotPassword),
    confirm: promisifyRoutine(confirm),
    editPassword: promisifyRoutine(editPassword),

}
