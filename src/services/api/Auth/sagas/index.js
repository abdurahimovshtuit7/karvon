import { all } from "redux-saga/effects";

import Login from './Login'
import confirm from './confirm'
import forgotPassword from './forgotPassword'
import signUp from './signUp'
import editPassword from "./editPassword";


export default function * sagas (api) {
    yield all(
        [
            Login(api),
            signUp(api),
            forgotPassword(api),
            confirm(api),
            editPassword(api)
        ]
    )
}
