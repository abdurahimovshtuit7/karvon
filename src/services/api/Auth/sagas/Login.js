import { call, all, put, takeEvery } from 'redux-saga/effects'

import { Login } from '../routines'
import {get} from 'lodash'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.log(JSON.stringify(request));
    try {
        yield put(Login.request());

        const response = yield call(api.auth.Login, request);
        console.log(JSON.stringify(response.data));
        yield all([
            call(TokenStorage.set, get(response.data,'data.access_token','')),
            put(
                Login.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        console.log(e);
        yield put(Login.failure(e))
    } finally {
        yield put(Login.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(Login.TRIGGER, trigger, api)
}
