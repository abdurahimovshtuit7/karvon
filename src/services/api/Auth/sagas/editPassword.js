import { call, put, takeEvery, all } from 'redux-saga/effects'

import { editPassword } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(editPassword.request());
       // const currentToken = yield call(TokenStorage.get)

        if(request.headers.phone){
            yield call(api.setPhone,request.headers.phone)
        }
        if(request.headers.code)
            yield call(api.setKod,request.headers.code)

        yield call(api.removeToken)


        const response = yield call(api.auth.editPassword, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                editPassword.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(editPassword.failure(e))
    } finally {
        yield put(editPassword.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(editPassword.TRIGGER, trigger, api)
}
