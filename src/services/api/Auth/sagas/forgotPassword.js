import { call, put, takeEvery, all } from 'redux-saga/effects'

import { forgotPassword } from '../routines'
import TokenStorage from '../../../TokenStorage';

function * trigger (api, action) {
    const { request } = action.payload;
    // console.warn(JSON.stringify(action.payload));
    try {
        yield put(forgotPassword.request());

        const response = yield call(api.auth.forgotPassword, request);
        console.warn(JSON.stringify(response));
        yield all([
            put(
                forgotPassword.success({
                    request,
                    response
                })
            )
        ])
    } catch (e) {
        yield put(forgotPassword.failure(e))
    } finally {
        yield put(forgotPassword.fulfill())
    }
}

export default function * (api) {
    yield takeEvery(forgotPassword.TRIGGER, trigger, api)
}
