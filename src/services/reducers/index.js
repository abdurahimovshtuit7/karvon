import { combineReducers } from 'redux';
import {LOGOUT} from "../constants";

/* ------------- Reducers ------------- */

import profile from "./profile";
import ads from "./ads";
import language from "./language";
import user from './user'

/* ------------- Reducers ------------- */

const rootReducer = combineReducers({
    profile,
    ads,
    language,
    user,
});

export default (state, action) => (
    action.type === LOGOUT ? rootReducer(undefined, action) : rootReducer(state, action)
)
