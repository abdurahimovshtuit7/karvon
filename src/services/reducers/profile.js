import {
   Login,
    signUp,
    forgotPassword,
    confirm,
    editPassword
} from '../api/Auth/routines'
import { SET_LANGUAGE, LOGOUT } from '../constants'
import TokenStorage from '../TokenStorage'
import get from 'lodash/get'

const initial = {
    Login: {},
    signUp: {},
    edit: {},
    info:{}
};

let pretty_message = (_message = {}, token) => {
    _message._id = _message.id;
    _message.text = _message.message;
    _message.createdAt = _message.created_at;
    _message.user = {};
    if(_message.type === 1){
        _message.user._id = 0;
    }
    else {
        _message.user._id = 1;
    }
    return _message
};

export default (state = initial, action) => {
    switch (action.type){
        case Login.SUCCESS:{
            let data = get(action, 'payload.response.data', {});
            return {
                ...state,
                Login: data
            }
        }
        case signUp.SUCCESS:{
            let data = get(action, 'payload.response.data', {});

            // console.log("getAllData", data)

            return {
                ...state,
                signUp: data
            }
        }
        case forgotPassword.SUCCESS:{
            let data = get(action, 'payload.response.data', {});

            console.log("getAllData", data)

            return {
                ...state,
                info: data
            }
        }
        case confirm.SUCCESS:{
            let Token = get(action, 'payload.response.data', {});

            // console.log("getTOKEN", Token)

            return {
                ...state,
                token: Token
            }
        }
        case editPassword.SUCCESS:{
            let data = get(action, 'payload.response.data', {});


            return {
                ...state,
              edit:data
            }
        }
        case SET_LANGUAGE:{
            return {
                ...state,
                currentLangCode: action.lng
            }
        }
        case LOGOUT: {
            TokenStorage.clear();
            return initial
        }
    }
    return state
}
