import {SET_LANGUAGE,SAVE_USER} from "../constants"

export const changeLanguage = lang => ({
    type:SET_LANGUAGE,
    lang,
});


export const saveUser = user => ({
    type:SAVE_USER,
    user,
})
