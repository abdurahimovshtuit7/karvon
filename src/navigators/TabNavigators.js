import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";

// import {createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {
    TouchableOpacity,
    Text
} from 'react-native'

import {get} from 'lodash'
import {connect} from "react-redux";
const Stack = createStackNavigator();
const Tab = AnimatedTabBarNavigator();
import Icon from 'react-native-vector-icons/Feather';
import Home from 'assets/images/SVG/Tabs/Home'
import SettingsIcon from 'assets/images/SVG/Tabs/Settings'
// import Icons from 'react-native-vector-icons/Ionicons'

import NavigationService from "./NavigationService";
import {withTranslation} from "react-i18next";
import colors from 'assets/styles/colors'
//------Screens------------
import HomeScreen from "screens/Home/HomeScreen";
import Statistics from 'screens/Home/Statistics'
import BookMarks from 'screens/Home/BookMarks'
import Notifications from 'screens/Home/Notifications'
import Settings from 'screens/Home/Settings'
import HomeStack from './HomeStack';


let TabNav = (props) => (
    <Tab.Navigator
        tabBarOptions={{
            activeTintColor: "#fff",
            inactiveTintColor: colors.inActive,
            activeBackgroundColor:colors.brandColor,
            labelStyle:{
                fontSize:14,
                fontFamily:'Raleway-Medium',
                // marginVertical:3
            }
        }}
        appearence={{
            // floating: true,
            topPadding: 10,
            // dotSize:'medium',
            horizontalPadding: 14,
        }}
    >
        <Tab.Screen name="home"
                    component={HomeStack}
                    options={({route}) => ({

            tabBarLabel: 'Home',
                        tabBarIcon: ({ focused, color, size }) => (
                            <Home
                                // name="home"
                                size={focused ? size : 26}
                                color={focused ? color : colors.inActive}
                                focused={focused}
                                // color={color}
                            />
                        )
        })}
        />
        <Tab.Screen name="statistics"
                    component={Statistics}
                    options={({route}) => ({
                        tabBarLabel: 'Statistic',
                        labelStyle:{

                        },
                        tabBarIcon: ({ focused, color, size }) => (
                            <Icon
                                name="bar-chart"
                                size={focused ? size : 26}
                                color={focused ? color : colors.inActive}
                                focused={focused}
                                // color={color}
                            />
                        )
                    })}
        />
        <Tab.Screen name="bookMarks"
                    component={BookMarks}
                    options={({route}) => ({
                        tabBarLabel: 'Marks',
                        tabBarIcon: ({ focused, color, size }) => (
                            <Icon
                                name="bookmark"
                                size={focused ? size : 26}
                                color={focused ? color : colors.inActive}
                                focused={focused}
                                // color={color}
                            />
                        )
                    })}

        />
        <Tab.Screen name="notifications"
                    component={Notifications}
                    options={({route}) => ({
                        tabBarLabel: 'Bell',
                        tabBarIcon: ({ focused, color, size }) => (
                            <Icon
                                name="bell"
                                size={focused ? size : 26}
                                color={focused ? color :colors.inActive}
                                focused={focused}
                                // color={color}
                            />
                        )

                    })}
        />
        <Tab.Screen name="settings"
                    component={Settings}
                    options={({route}) => ({
                        tabBarLabel: 'Settings',
                        tabBarIcon: ({ focused, color, size }) => (
                            <SettingsIcon
                                // name="settings"
                                size={focused ? size : 26}
                                color={focused ? color : colors.inActive}
                                focused={focused}
                                // color={color}
                            />
                        )
                    })}
        />


    </Tab.Navigator>
)


const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        // user:state.user.user,

    };
};

TabNav = connect(mapStateToProps)(TabNav)
export default withTranslation('main')(TabNav);
