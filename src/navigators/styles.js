import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'
import {center} from '../assets/styles/commons';

const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
        // borderWidth:1,
        // backgroundColor:'rgba(211,255,51,0.1)',
        paddingBottom:15,
        marginTop:0,
    },
    // headerLeft:{
    //     margin:10,
    //     // borderWidth:1,
    // },
    wrapper: {
        height: "100%",
        //borderRadius:10
        // paddingBottom:10
        // backgroundColor:'rgba(255, 0, 255, 1.0)'

    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    text:{
        marginRight: 15,
        paddingLeft: 20,
        marginTop:10,
        fontSize:16,
        fontWeight:'bold',
        color:'#464646'
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
    },
    row: {
        marginTop: 20,
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        paddingHorizontal:10
    },

    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },

    drawerSection: {
        marginTop: 15,
    },
    touchable1:{
        backgroundColor:colors.yellow ,
        alignItems:'center',
        paddingVertical: 8,
        borderRadius:15,
        borderWidth: 1,
        borderColor: colors.white,
        // elevation:6,
         width:'35%',

    },
    button:{
      // borderWidth:1,
        width:'80%',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:8,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: colors.white,
        backgroundColor: colors.brandColor
    },
    button1:{
      // borderWidth:1,
        width:'60%',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:8,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: colors.white,
        backgroundColor: colors.brandColor
    },
    text1:{
      color:colors.white,
        fontSize:14,
        fontFamily:'Raleway-Bold',
        // paddingHorizontal: 3,
    },
    text2:{
      fontSize:14,
      fontFamily:'Raleway-Bold',
    },

    image:{
        flex:1,
        paddingVertical:15,
        justifyContent:'center',
        alignItems:'center',
        // borderWidth: 1
    },
    container:{
        justifyContent:'center'
    },
    brand:{
        fontSize:24,
        alignSelf:'center',
        color:colors.white,
        fontFamily:'Raleway-Bold'
    },
    avatar:{
        // ...center,
        width: 80,
        height: 80,
        borderRadius: 50,
        borderWidth:2,
        borderColor:colors.brand,

    },
    username:{
        alignSelf: 'center',
        fontSize:16,
        color:colors.gray,
        fontFamily: "Poppins-Bold"
    },
    rows: {
        paddingHorizontal:27,
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginBottom: 10
    },
    rows1: {
        paddingHorizontal:20,
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    touchable:{
        // backgroundColor:'#c7c7c7' ,
        paddingHorizontal:30,
        paddingVertical: 10,
        borderRadius:10,
        elevation:6,
        //  width:'50%',

    },
    langText:{
        color:'#fff',
        fontWeight:'bold'
    }
});
export default styles

