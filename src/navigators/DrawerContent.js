import React, {Component, useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    AsyncStorage,
    Linking, ImageBackground,
} from 'react-native';
import {
    useTheme,
    Avatar,
    Title,
    Caption,
    Paragraph,
    Drawer,
    Text,
    TouchableRipple,
    Switch,
} from 'react-native-paper';

import {
    DrawerContentScrollView,
    DrawerItem,
    DrawerItemList,

} from '@react-navigation/drawer';
import Components from 'components';
import {connect} from 'react-redux';
import {withTranslation} from 'react-i18next';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icons from 'react-native-vector-icons/Entypo';
import styles from './styles';
import NavigationService from './NavigationService';
import LinearGradient from 'react-native-linear-gradient';
import {get} from 'lodash';
import {saveUser} from 'services/actions';
import colors from '../assets/styles/colors';
import {SET_LANGUAGE} from "services/constants";



class DrawerContent extends Component {
    onLangClicked = (lang) => {
        // console.log("LANG:",lang)
        let {i18n,dispatch} = this.props;
        i18n.changeLanguage(lang,()=>{
            dispatch({type:SET_LANGUAGE,lang})
        })
    }
    render() {
        let {userData,t,currentLangCode} = this.props;
        console.log("USER_DATA:",get(userData, 'token', ''))
        return (
            <View style={{flex: 1, marginTop: -4}}>
                <DrawerContentScrollView {...this.props}>
                    <View style={styles.drawerContent}>
                        {/*<LinearGradient colors={['rgba(78, 125, 241,1)', 'rgba(255, 255, 255,1)']}*/}
                        {/*                style={styles.wrapper}*/}
                        {/*>*/}
                        {/*    <Text style={styles.brand}>Karvon</Text>*/}
                            <View style={styles.image}>
                                <Components.UserImage username={''} style={styles.avatar}
                                                      uri={get(userData, 'token', '')?(require('assets/images/Ellipse.png')):null}/>
                            </View>
                            {
                                (!get(userData, 'token', '')) ? (<View style={styles.row}>
                                    <TouchableOpacity style={styles.button} onPress={() => {
                                        NavigationService.navigate('auth');
                                    }}>
                                        <Text style={styles.text1}>
                                            {t('Royxatdan_utish')}
                                        </Text>
                                    </TouchableOpacity>
                                    {/*<TouchableOpacity style={styles.touchable}>*/}
                                    {/*    <Text style={styles.text2}>Log Out</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                </View>) : (
                                    <>
                                        <Text style={styles.username}>Abdurahimov Shaxboz</Text>

                                        <View style={styles.rows1}>
                                            <TouchableOpacity style={styles.button1} onPress={() => {
                                                NavigationService.navigate('auth');
                                            }}>
                                                <Text style={styles.text1}>
                                                    {t('Profile')}
                                                </Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.touchable1} onPress={() => {
                                                this.props.dispatch(saveUser({}));
                                                // BackHandler.exitApp();
                                            }}>
                                                <Text style={styles.text2}>{t('Log_Out')}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </>
                                )
                            }


                        {/*</LinearGradient>*/}
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            {...this.props}
                            icon={({color, size}) => (
                                <Icons
                                    name="images"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Categories 1"
                            labelStyle={{
                                fontFamily: 'Poppins-SemiBold',
                            }}
                            // onPress={() => {this.props.navigation.navigate('login')}}
                        />
                        <DrawerItem
                            {...this.props}
                            icon={({color, size}) => (
                                <Icons
                                    name="images"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Categories 2"
                            labelStyle={{
                                fontFamily: 'Poppins-SemiBold',
                            }}
                            // onPress={() => {this.props.navigation.navigate('login')}}
                        />
                        <DrawerItem
                            {...this.props}
                            icon={({color, size}) => (
                                <Icons
                                    name="images"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Categories 3"
                            labelStyle={{
                                fontFamily: 'Poppins-SemiBold',
                            }}
                            // onPress={() => {this.props.navigation.navigate('login')}}
                        />
                        <DrawerItem
                            {...this.props}
                            icon={({color, size}) => (
                                <Icons
                                    name="images"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Categories 4"
                            labelStyle={{
                                fontFamily: 'Poppins-SemiBold',
                            }}
                            // onPress={() => {this.props.navigation.navigate('login')}}
                        />

                    </Drawer.Section>
                    <View>
                        <Text style={styles.text}>{t('Til_tanlash')}</Text>
                        <View style={styles.rows}>
                            <TouchableOpacity style={[styles.touchable,{backgroundColor:(currentLangCode==='uz')?colors.brandColor:'#c7c7c7'}]} onPress={() => {
                                this.onLangClicked('uz')
                            }}>
                                <Text style={styles.langText}>
                                    UZB
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.touchable,{backgroundColor:(currentLangCode==='ru')?colors.brandColor:'#c7c7c7'}]} onPress={() => {
                                this.onLangClicked('ru')
                            }}>
                                <Text style={styles.langText}>
                                    РУС
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </DrawerContentScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        currentLangCode:state.language.lang,
        userData:state.user.user
    };
};

DrawerContent = connect(mapStateToProps)(DrawerContent);
export default withTranslation('main')(DrawerContent);
