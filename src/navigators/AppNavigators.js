import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import NavigationService from './NavigationService';


//------------SCREENS--------------

import Login from 'screens/Auth/Login';
import SelectAuth from 'screens/Auth/SelectAuth';
import SignUp from 'screens/Auth/SignUp';
import ForgotPassword from 'screens/Auth/ForgotPassword';
import SmsVerify from 'screens/Auth/SmsVerify';
import DrawerContent from './DrawerContent';
import HomeScreen from 'screens/Home/HomeScreen';
import Settings from 'screens/Home/Settings';

import {connect} from 'react-redux';
import LaunchScreen from 'screens/LaunchScreen/index';
import DrawerNav from './DrawerNavigator';
import TabNav from './TabNavigators';
import i18next from 'i18next';
import {CommonActions} from '@react-navigation/native';
import {Platform, StatusBar} from 'react-native';
import colors from '../assets/styles/colors';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();


const Auth = (props) => (
    <Stack.Navigator>

        {/*<Stack.Screen name='home' component={TabNav}*/}
        {/*              options={{*/}
        {/*                  headerShown: false,*/}
        {/*              }}*/}
        {/*/>*/}
        <Stack.Screen name='login' component={Login}
                      options={{
                          headerShown: false,
                      }}
        />
        <Stack.Screen name='selectAuth' component={SelectAuth}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='signUp' component={SignUp}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='forgotPassword' component={ForgotPassword}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        <Stack.Screen name='smsVerify' component={SmsVerify}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
    </Stack.Navigator>
);


class AppNavigators extends Component {
    state={
        isLoading:true
    }

    render() {
        // if (this.state.isLoading) {
        //     // We haven't finished checking for the token yet
        //     return <LaunchScreen loading={}/>;
        // }
        return (
            <Stack.Navigator>
                <Stack.Screen name='launchScreen' component={LaunchScreen}
                              options={{
                                  gestureEnabled:true,
                                  headerShown: false,
                              }}
                />
                <Stack.Screen name='drawer' component={DrawerNav}
                              options={({route}) => ({
                                  headerShown: false,
                              })}
                />
                <Stack.Screen name='auth' component={Auth}
                              options={({route}) => ({
                                  headerShown: false,
                              })}
                />

            </Stack.Navigator>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        userData: state.user.user,
    };
};

export default connect(mapStateToProps)(AppNavigators);
