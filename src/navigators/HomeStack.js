import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {
    TouchableOpacity
} from 'react-native';


import HomeScreen from "screens/Home/HomeScreen";

import HeaderLeft from 'assets/images/SVG/headerLeft'
import HeaderRight from 'assets/images/SVG/headerRight'

import {connect} from "react-redux";
import colors from '../assets/styles/colors';
import styles from './styles'
import SingleListing from 'screens/Home/SingleListing';

const Stack = createStackNavigator();

const HomeStack = (props) => (
    <Stack.Navigator initialRouteName='home'>
        <Stack.Screen name='home' component={HomeScreen}
                      options={{
                          title: 'Karvon',
                          headerStyle: {
                              backgroundColor: colors.brandColor,
                          },
                          headerTintColor: '#fff',
                          headerTitleStyle: {
                             fontFamily:'Raleway-SemiBold',
                              fontSize:27,
                              alignSelf:'center'
                          },
                          headerLeft: () => (
                              <TouchableOpacity
                                  onPress={() => props.navigation.toggleDrawer()}
                                  // title="Info"
                                  // color="#fff"
                                  style={styles.headerLeft}
                              >
                                  <HeaderLeft/>
                              </TouchableOpacity>),
                          headerLeftContainerStyle:{
                              padding:10,
                              // borderWidth:1,
                          },
                          headerRight:() => (
                              <TouchableOpacity
                                  onPress={() => props.navigation.toggleDrawer()}
                                  // title="Info"
                                  // color="#fff"
                                  style={styles.headerLeft}
                              >
                                  <HeaderRight/>
                              </TouchableOpacity>),
                          headerRightContainerStyle:{
                              padding:10,
                              // borderWidth:1,
                          },
                      }}
        />
        <Stack.Screen name='singleListing' component={SingleListing}
                      options={({route}) => ({
                          headerShown: false,
                      })}
        />
        {/*<Stack.Screen name='category' component={Category}*/}
        {/*              options={({route}) => ({*/}
        {/*                  headerShown: false,*/}

        {/*              })}*/}
        {/*              backTitleVisible={true}*/}
        {/*/>*/}

    </Stack.Navigator>
)

const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data
    };
};

export default connect(mapStateToProps)(HomeStack);
