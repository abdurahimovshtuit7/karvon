import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";

// import {createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import {
    TouchableOpacity,
    Text
} from 'react-native'

import {get} from 'lodash'
import {createDrawerNavigator} from '@react-navigation/drawer';
import TabNav from './TabNavigators';
import {connect} from "react-redux";


import NavigationService from "./NavigationService";
import {withTranslation} from "react-i18next";

import colors from 'assets/styles/colors'
//------Screens------------
import Settings from 'screens/Home/Settings'
import DrawerContent from './DrawerContent';

const Drawer = createDrawerNavigator();


let DrawerNav = (props) => (
    <Drawer.Navigator
        drawerStyle={{
            backgroundColor: '#ffffff',
        }}
        drawerContent={props => <DrawerContent {...props} />}
        // hideStatusBar={true}
    >
        <Drawer.Screen name="home" component={TabNav}
                       options={{
                           headerShown: false,
                       }}
        />
        <Drawer.Screen name="profil"
                       component={Settings}
                       options={({route}) => ({
                           //title: route.params.title,
                           headerStyle: {
                               backgroundColor: '#6F90B7',
                           },
                           headerTintColor: '#fff',
                       })}
        />
    </Drawer.Navigator>
)


const mapStateToProps = (state, ownProps) => {
    return {
        // tabButton: state.tabButton.data,
        // user:state.user.user,

    };
};

DrawerNav = connect(mapStateToProps)(DrawerNav)
export default withTranslation('main')(DrawerNav);
