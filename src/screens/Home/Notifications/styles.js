import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:colors.white

    },
   text:{
        textAlign:'center',
        fontSize:24,

   }

})

export default styles
