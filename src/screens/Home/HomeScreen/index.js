import React, {Component, useState} from 'react';
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar,
    View,
    TextInput,
    ScrollView,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup';
import {withTranslation} from 'react-i18next';

import styles from './styles';
import colors from 'assets/styles/colors';
import Components from 'components';
import {Routines} from 'services/api';
import {get} from 'lodash';
import {connect} from 'react-redux';
// import {saveUser} from "services/actions";
import NavigationService from 'navigators/NavigationService';
// import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from 'react-native-confirmation-code-field';
import Search from 'assets/images/SVG/Search';

class HomeScreen extends Component {

    render() {
        const {userData,t} = this.props
        console.log("DATA:",userData)
        // console.log("PROPS:",this.props)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.brandColor} barStyle="light-content" translucent={false}/>
                <TouchableOpacity style={styles.search}>
                    <Text style={styles.searchStyle}>
                        Search...
                    </Text>
                    <Search/>
                </TouchableOpacity>

                <ScrollView contentContainerStyle={styles.scroll} >
                    <Components.SliderImage/>
                    <Components.CategoriesList/>
                    <View style={styles.padding}>
                    <Components.TextWithBorder text={t('top')} style={styles.borderText}/>
                    <Components.Item height={176} uri={require('assets/images/main.jpg')} touch={()=>{NavigationService.navigate('singleListing')}}/>
                    <View style={styles.row}>
                        <Components.Item width={'49%'} height={128} touch={()=>{NavigationService.navigate('singleListing')}}/>
                        <Components.Item width={'49%'} height={128} uri={require('assets/images/buy.jpg')} touch={()=>{NavigationService.navigate('singleListing')}}/>
                    </View>
                    <View style={styles.rows}>
                        <Components.TextWithBorder text={t('mutaxassis_blog')}/>
                        <Text style={styles.blueText}>{t('hammasi')}</Text>
                    </View>
                    <Components.Poster uri={require('assets/images/group.jpg')}
                                       touch={()=>{NavigationService.navigate('singleListing')}}
                                       share={t('Share')}
                    />
                    <Components.Poster uri={require('assets/images/team.jpg')}
                                       touch={()=>{NavigationService.navigate('singleListing')}}
                                       share={t('Share')}
                    />
                    <Components.Poster touch={()=>{NavigationService.navigate('singleListing')}}
                                       share={t('Share')}
                    />
                    </View>
                </ScrollView>

            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        userData:state.user.user
    };
};
HomeScreen = connect(mapStateToProps)(HomeScreen);
export default withTranslation('main')(HomeScreen);
