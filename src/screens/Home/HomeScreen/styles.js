import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor:colors.white
    },
   text:{
        textAlign:'center',
        fontSize:24,
   },
    search: {
        marginHorizontal: 15,
        marginVertical: 10,
        paddingHorizontal: 14,
        paddingVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
        backgroundColor: colors.background,
        borderRadius: 5
    },
    searchStyle: {
        fontSize: 14,
        // paddingLeft: 10,
        fontFamily:'Poppins-Regular',
        color: colors.inActive,
    },
    scroll:{
        // paddingHorizontal: 14,
        paddingBottom:40
    },
    row:{
        flexDirection: 'row',
        // flex:1,
        justifyContent: 'space-between'
        // height:'50%'
    },
    padding:{
        paddingHorizontal: 14
    },
    rows:{
        flexDirection: 'row',
        // flex:1,
        marginTop:15,
        marginBottom:4,
        justifyContent: 'space-between'
        // height:'50%'
    },
    borderText:{
        paddingVertical: 5,
        textTransform: 'uppercase'
    },
    blueText:{
        fontSize:14,
        fontFamily: 'Raleway-Regular',
        color:colors.brandColor
    }

})

export default styles
