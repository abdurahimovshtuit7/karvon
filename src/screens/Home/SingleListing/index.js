import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Image,
    ImageBackground,
    SafeAreaView,
    StatusBar, Dimensions,
    RefreshControl
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather'
import {get} from 'lodash'
// import Swiper from "react-native-swiper";
import HeaderImageScrollView, {TriggeringView} from 'react-native-image-header-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import colors from "assets/styles/colors";
import styles from "./styles";
import {connect} from "react-redux";
import {withTranslation} from "react-i18next";
import Icn from 'react-native-vector-icons/FontAwesome'
import Icons from 'react-native-vector-icons/FontAwesome5'
import Iconos from 'react-native-vector-icons/EvilIcons'
import {Routines} from "services/api";
import Components from "components";
import NavigationService from "navigators/NavigationService";
// import MapView from 'react-native-maps';

//----------SVG-------------
import Share from 'assets/images/SVG/Share'
import ArrowBack from 'assets/images/SVG/ArrowBack'
import Comment from 'assets/images/SVG/comment';
import Human from 'assets/images/SVG/human'
import Heart from 'assets/images/SVG/Heart'
import Calendar from 'assets/images/SVG/Calendar'
// import BookMarks from 'assets/images/SVG/Save'
// import Warning from 'assets/SVG/Warning'

const windowWidth = Dimensions.get('window').width;

const OFFSET=100
class SingleListing extends Component {
    state = {
        refreshing: false,
    }
    componentDidMount() {
        const _scrollView = this.scrollView;
        _scrollView.scrollTo({y: 100});
    }
    _onRefresh() {
        this.setState({
            refreshing: true,
            currentIndex:2,
        });

        setTimeout(() => {
            this.setState({
                refreshing: false,
            });
        }, 2000);
    }
    handleScroll (event: Object) {
    console.log(event.nativeEvent.contentOffset.y);
}

    _onMomentumScrollEnd = ({ nativeEvent }: any) => {
        // the current offset, {x: number, y: number}
        const position = nativeEvent.contentOffset;
        // page index
        const index = Math.round(nativeEvent.contentOffset.x / PAGE_WIDTH);

        if (index !== this.state.currentIndex) {
            // onPageDidChanged
        }
    };

    render() {
        const {navigation, route, items} = this.props
        // console.log("ITEMS:", items)
        // const data = get(this.props.route.params, 'item', {})
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor="#000" barStyle="ligth-content" translucent={false}/>

                <HeaderImageScrollView
                    maxHeight={300}
                    minHeight={60}
                    scrollViewBackgroundColor="transparent"
                    scrollViewStyle={{
                        borderTopRightRadius:40,
                        // backgroundColor:'green'
                    }}
                    disableHeaderGrow={true}
                    minOverlayOpacity={0.1}
                    maxOverlayOpacity={0.1}
                    bounces={false}
                    // headerContainerStyle={styles.header}
                    // useNativeDriver={true}
                    // headerImageStyle={{
                    //     borderWidth:5,
                    // }}
                    // headerImage={require("assets/images/girls.png")}
                    // renderForeground={() => (
                    //     <View style={{ height: 600, justifyContent: "center", alignItems: "center" }} >
                    //         <TouchableOpacity onPress={() => console.log("tap!!")}>
                    //             <Text style={{ backgroundColor: "transparent" }}>Tap Me!</Text>
                    //         </TouchableOpacity>
                    //     </View>
                    // )}
                    renderHeader={() => <Image source={require("assets/images/girls.png")} style={{ height: 300, width: Dimensions.get('window').width,zIndex:-1 }} />}
                    // refreshControl={
                    //     <RefreshControl
                    //         refreshing={this.state.refreshing}
                    //         onRefresh={this._onRefresh.bind(this)}
                    //         tintColor="white"
                    //     />
                    // }
                    // renderTouchableFixedForeground={()=>(
                    //     <View>
                    //         <Ionicons name="arrow-back" size={25} style={{marginLeft: 20,marginTop:20,position:'absolute'}} onPress={() => {
                    //             navigation.goBack()
                    //         }}/>
                    //     </View>
                    // )}
                    fadeOutForeground={true}
                    // foregroundParallaxRatio={3}
                    headerContainerStyle={{
                        backgroundColor:'white',
                        zIndex:-1,
                        paddingBottom:-100,
                        flex:1
                    }}
                    fixedForegroundContainerStyles={{
                        top:-10,
                        marginBottom:-100,
                        bottom:100,
                        // flex:1,
                        zIndex:1,
                        position:'absolute'
                        // backgroundColor:'white'
                    }}
                    renderTouchableFixedForeground={() => (
                        <LinearGradient colors={['rgba(0,0,0,0.5)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)', 'rgba(0, 0, 0, 0)']}
                                        style={styles.wrapper}>
                        <View style={styles.containerS}>

                            <ArrowBack name="arrow-back" size={25}
                                      style={{marginLeft: 20, marginTop: 20, position: 'absolute', zIndex: 1}}
                                      color={colors.white} onPress={() => {
                                navigation.goBack()
                            }}/>
                            {/*<LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>*/}

                            <View style={styles.containerS}>

                                        {/*<ImageBackground source={require('assets/images/girls.png')}*/}
                                        {/*                 style={styles.images}>*/}
                                        {/*    <LinearGradient colors={['rgba(0,0,0,0.5)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)','rgba(0,0,0,0)', 'rgba(0, 0, 0, 0)']}*/}
                                        {/*                    style={styles.wrapper}/>*/}
                                        {/*</ImageBackground>*/}
                                <Icon
                                    name="bookmark"
                                    size={26}
                                    style={{right: 15, top: 22, position: 'absolute', zIndex: 1}}
                                    color={colors.white}
                                />
                                <Share style={{right: 60, top: 20, position: 'absolute', zIndex: 1}}
                                       color={colors.white}
                                       size={26}
                                />

                                {/*<SaveIcon style={{right: 15, top: 22, position: 'absolute', zIndex: 1}}*/}
                                {/*          color={colors.white}/>*/}
                            </View>
                            {/*</LinearGradient>*/}

                        </View>
                        </LinearGradient>
                    )}
                >
                    <ScrollView contentContainerStyle={[styles.scroll,{paddingTop: Platform.select({android: 10, ios: 0})}]}
                                // contentOffset={{x:100,y:100}}
                                ref={scrollView => this.scrollView = scrollView}
                                // scrollIndicatorInsets={{ right: 1 }}
                                // pagingEnabled={true}
                                // onMomentumScrollEnd={this._onMomentumScrollEnd}
                                // scrollEventThrottle={160}
                                contentInset={{ top: OFFSET }}
                                contentOffset={{ y: OFFSET }}
                                // automaticallyAdjustContentInsets={true}
                                // onScroll={this.handleScroll}
                    >

                        <TriggeringView  style={styles.trigger} onHide={() => console.log("text hidden")}>
                            <Text style={styles.text}>
                                The news our features
                                dolor
                            </Text>
                            <View style={styles.comment}>
                                <View style={styles.addRow}>
                                    <Icons name={'user-alt'} size={17}/>
                                    <Text style={styles.number}>
                                        Admin Elezone
                                    </Text>

                                </View>
                                <View style={styles.addRow}>
                                    <Iconos name={'calendar'} size={26}/>
                                    <Text style={styles.number}>
                                        20.08.2020
                                    </Text>
                                </View>
                                <View style={styles.addRow}>
                                    <Icn name={'comments'} size={20}/>
                                    <Text style={styles.number}>
                                        25 Comments
                                    </Text>
                                </View>
                            </View>

                            <Text style={styles.text1}>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                            </Text>
                            <Text style={styles.text1}>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                            </Text>
                            <Text style={styles.text1}>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                            </Text>
                            <Text style={styles.text1}>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                            </Text>
                            <Text style={styles.text1}>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                            </Text>

                            {/*<Components.SendMessage/>*/}
                            {/*<View style={styles.wrap}>*/}
                            {/*    <Components.Frame text={data.type}/>*/}
                            {/*    <Components.Frame text={'Number of bedroms:'} number={'3'}/>*/}
                            {/*    <Components.Frame text={'Funished'} number={'Yes'}/>*/}
                            {/*    <Components.Frame text={'Number of floors:'} number={'6'}/>*/}
                            {/*    <Components.Frame text={'Floor:'} number={'3'}/>*/}
                            {/*    <Components.Frame text={'Size:'} number={'1400 Sqft'}/>*/}
                            {/*</View>*/}


                            {/*<View style={styles.button}>*/}
                            {/*    <Components.Owner name={'Ivan Petrov'} listings={'More listings: 8'} status={'online'}*/}
                            {/*                      touch={() => {*/}
                            {/*                          NavigationService.navigate('owner', {title: "Listing Owner"});*/}
                            {/*                      }}/>*/}

                            {/*</View>*/}


                        </TriggeringView>

                    </ScrollView>
                </HeaderImageScrollView>
            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        // items: state.ads.items,
        // categories: state.ads.categories,
        // currentLangCode: state.language.lang,
        // favoritesList: state.ads.favoritesList,
        // favorites: state.ads.Favorite
    };
};

SingleListing = connect(mapStateToProps)(SingleListing)
export default withTranslation('main')(SingleListing)
