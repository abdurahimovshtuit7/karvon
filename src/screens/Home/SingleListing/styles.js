import {StyleSheet, Platform, Dimensions} from 'react-native';
import colors from 'assets/styles/colors'
import {horizontal} from "assets/styles/commons";
var {height, width} = Dimensions.get('window');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginVertical:10,
        backgroundColor:colors.white,
        // borderWidth:1,
        // justifyContent:'center',
    },
    linearGradient: {
        flex: 1,
    },
    header:{
        // backgroundColor:'green',
        // borderBottomLeftRadius:40,
    },
    view:{
        flex:1,
        // flex: 0.48,
        flexDirection: 'row',
        justifyContent:'space-between',
        // marginHorizontal: 9,
        flexWrap:'wrap'
    },
    row:{
        flexDirection:'row',
        // flex:1
        justifyContent: 'space-between',
        paddingHorizontal:18,
        paddingTop:16,
        paddingBottom:10
    },
    text:{
        fontSize:27,
        marginTop:20,
        // fontWeight:'bold',
        color:colors.primary,
        fontFamily: 'Poppins-SemiBold',
        paddingHorizontal: 15,
        // textAlign:'center'
    },
    text1:{
       fontSize: 12,
       marginVertical:10,
        color:colors.primary,
        paddingHorizontal: 15,
        lineHeight:20
    } ,

    containerS:{
        // borderWidth:1,
        // marginTop:13,
        flex:1,
        // zIndex:-1,
        // backgroundColor:'green'

    },
    wrapper: {
        height: "100%",
        //borderRadius:10
        // paddingBottom:10
        // backgroundColor:'rgba(255, 0, 255, 1.0)'

    },

    Pagination:{
        bottom:12,

    },
    images:{
        width:width,
        // height:"100%",
        // borderRadius: 15,
        overflow: "hidden",
        // backgroundColor:'rgba(255, 0, 255, .3)'

    },
    scroll:{
        // height:1000,
        // borderWidth:1,
        // overflow:'visible',
        borderTopLeftRadius:45,
        borderTopRightRadius:45,
        zIndex:2,
        // position: 'absolute',
        // top:-30,
        // bottom:50,
        paddingBottom:20,
        backgroundColor:colors.white

    } ,
    wrap:{
        flexWrap: 'wrap',
        flexDirection:'row',
        paddingHorizontal: 15,
    },
    description: {
        fontSize: 14,
        paddingVertical: 15,
        lineHeight: 24,
        paddingHorizontal: 15,
    },
    text2:{
        fontSize:12,
        color:'white',
        fontFamily:'arial',
        position:'absolute',
        backgroundColor:`rgba(0, 0, 0, 0.3)`,
        padding:4,
        borderRadius:7,
        // bottom:600,
        alignSelf:'center',
        // top:Height*0.158,
        left:15
    },
    box:{
        // position: 'absolute',
        bottom:34,
        borderRadius:5,
    },
    button:{
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius:10,
        backgroundColor:colors.white,
        elevation:10,
        marginHorizontal:15,
        marginVertical: 15,
    },
    trigger:{
        flex:1
        // paddingTop:-30,
        // zIndex: 3
    },
    title:{
        marginTop: 10,
        paddingHorizontal:15,
        fontSize:18,
        color:colors.primary,
        fontWeight: 'bold',
    },
    horizontal:{
        paddingVertical:5,
        paddingHorizontal: 10,
        // marginRight:10,
    },
    footer:{
        borderTopWidth:1,
        borderTopColor:colors.border,
        marginTop:10,
        paddingHorizontal:15,
        paddingVertical:15,
    },
    footerRow:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingBottom: 20,
    },
    footerText:{
        color:colors.second,
    },
    footerB: {
        // flex:0.5,
        borderWidth: 1,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5,
        paddingHorizontal:10,
        borderColor:colors.border,
        // width:'50%'
        // borderWidth:1

    },
    buttonT:{
        marginHorizontal:10,
        paddingVertical:10,
        color:colors.second,
        // borderWidth:1,
    },
    footerWrap:{
        ...horizontal
    },
    items:{
        paddingHorizontal:4,
    },
    comment:{
        flexDirection: 'row',
        paddingHorizontal:14,
    },
    addRow:{
        flexDirection: 'row',
        paddingRight: 28,
        justifyContent:'center'
    },
    number:{
        paddingLeft:8,
        fontSize:11,
        fontFamily:'Raleway-SemiBold'
    },


})
export default styles
