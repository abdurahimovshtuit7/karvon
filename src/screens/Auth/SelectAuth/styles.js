import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white

    },
    welcome:{
        marginTop:'11%',
        textAlign:'center',
        fontSize:49,
        marginBottom: 11,
        fontFamily:'Raleway-Bold'
    },
    text:{
        textAlign:'center',
        fontSize:16,
        fontFamily: 'Raleway-Regular',
        color:'rgba(0,0,0,0.6)',
        paddingHorizontal:20,
        marginBottom:'5%'
    },
    component:{
        marginTop: '26%'
    }

})

export default styles
