import React, {Component} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar,
} from "react-native";
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
// import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import NavigationService from "navigators/NavigationService";
import Person from 'assets/images/SVG/Person'
import HandShake from 'assets/images/SVG/HandShake'



class SelectAuth extends Component {
    state = {
        selected:'',
    }
    Selected (id) {
        (id==='1')?(this.setState({selected:'personal'})):( this.setState({selected:'business'}))
    }
    render() {
        const {selected} = this.state
        const {t}=this.props
        console.log("DDDD:",selected)
        const PROP = [
            {
                key: '1',
                title: t('personal'),
                svg: <Person/>,
            },
            {
                key: '2',
                title: t('business'),
                svg: <HandShake/>,
            },
        ];
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <Components.Layout>
                    <Text style={styles.welcome}>
                        Karvon
                    </Text>
                    <Text style={styles.text}>
                        {t("profile_type")}
                    </Text>

                    <Components.SelectPerson PROP={PROP}
                                             onSelected={(id) => {
                                                 this.Selected(id);
                                             }}/>
                    <Components.Submit text={t('continue')}
                                       color={colors.white}
                                       background={colors.brandColor}
                                       touch={() => {
                                           // handleSubmit()
                                           (selected!=='')?(NavigationService.navigate('signUp',{
                                               auth:selected
                                           })):null
                                       }}
                                       style={styles.component}
                        // margin={'26%'}
                    />
                </Components.Layout>
            </SafeAreaView>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {};
};

SelectAuth = connect(mapStateToProps)(SelectAuth)
export default withTranslation('main')(SelectAuth)
