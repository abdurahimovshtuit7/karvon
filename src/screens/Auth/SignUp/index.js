import React, {Component} from 'react'
import {
    SafeAreaView,
    Text,
    View,
    TouchableOpacity,
    StatusBar,
} from "react-native";
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";
import {Formik, Field, Form, withFormik} from 'formik';
import Icon from 'react-native-vector-icons/Feather';
import CheckBox from '@react-native-community/checkbox';


import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import NavigationService from "navigators/NavigationService";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";


class SignUp extends Component {
    state = {
        item1:[
            {label: 'Узбекистан', value: 'uk'},
            {label: 'Казахстан', value: 'france'},
        ],
        item2:[
            {label: 'Ташкент', value: 'uk'},
            {label: 'Джизак', value: 'france'},
        ],
        item3:[
            {label: 'Юнусабадский', value: 'uk'},
            {label: 'Казахстан', value: 'france'},
        ],

    }

    render() {
        const {item1,item2,item3}=this.state
        const {navigation,handleSubmit,errors,route,t} = this.props
        console.log("params:",route.params.auth)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}>
                    <Components.Layout>
                        <Text style={styles.welcome}>
                            Karvon
                        </Text>
                        <Text style={styles.text}>
                            {t('registration')}
                        </Text>
                        <Field
                            name={'name'}
                            label={'name'}
                            title={t('Username')}
                            placeHolder={t('write_here')}
                            component={Components.InputText}
                        />
                        {errors.name ? (
                            <Text style={styles.error}>{errors.name}</Text>
                        ) : null}

                        {/*<Components.Picker title={'Введите адрес '}*/}
                        {/*                   zIndex={3}*/}
                        {/*                   items={item1}*/}
                        {/*                   placeholder={'Узбекистан'}*/}

                        {/*/>*/}
                        {/*<Components.Picker title={'Область '}*/}
                        {/*                   zIndex={2}*/}
                        {/*                   items={item2}*/}
                        {/*                   placeholder={'Ташкент'}*/}
                        {/*/>*/}
                        {/*<Components.Picker title={'Город '}*/}
                        {/*                   zIndex={1}*/}
                        {/*                   items={item3}*/}
                        {/*                   placeholder={'Юнусабадский'}*/}
                        {/*/>*/}
                        <Field
                            name={'phone'}
                            label={'phone'}
                            title={t('telefon_raqam')}
                            placeHolder={t('write_here')}
                            component={Components.InputText}
                        />
                        {errors.phone ? (
                            <Text style={styles.error}>{errors.phone}</Text>
                        ) : null}

                        <Field
                            name={'email'}
                            label={'email'}
                            title={t('email')}
                            placeHolder={t('write_here')}
                            component={Components.InputText}
                        />
                        {errors.email ? (
                            <Text style={styles.error}>{errors.email}</Text>
                        ) : null}
                        <Field
                            name={'password'}
                            label={''}
                            title={t('Parol')}
                            placeHolder={t('enter_password')}
                            component={Components.InputText}
                        />
                        {errors.password ? (
                            <Text style={styles.error}>{errors.password}</Text>
                        ) : null}
                        <Field
                            name={'confirmPassword'}
                            label={'confirmPassword'}
                            title={t('confirm_password')}
                            placeHolder={t('enter_password')}
                            component={Components.InputText}
                        />
                        {errors.confirmPassword ? (
                            <Text style={styles.error}>{errors.confirmPassword}</Text>
                        ) : null}
                        {/*<View style={styles.row}>*/}
                        {/*    <CheckBox*/}
                        {/*        value={true}*/}
                        {/*        disabled={false}*/}
                        {/*        style={{*/}
                        {/*            height: 19,*/}
                        {/*            width: 19,*/}
                        {/*            backgroundColor: 'transparent',*/}
                        {/*        }}*/}
                        {/*        boxType={'square'}*/}
                        {/*        onAnimationType={'one-stroke'}*/}
                        {/*        offAnimationType={'one-stroke'}*/}
                        {/*        boxSize={10}*/}
                        {/*        tintColor={colors.border}*/}
                        {/*    />*/}
                        {/*    <Text style={styles.checkLabel}>*/}
                        {/*        Я согласен с условиями использования сайта*/}
                        {/*</Text>*/}
                        {/*</View>*/}


                        <Components.Submit text={t('reg')}
                                           color={colors.white}
                                           background={colors.brandColor}
                                           style={styles.component}
                                           touch={() => {
                                               handleSubmit()
                                               // NavigationService.navigate('selectAuth')
                                           }}/>
                    </Components.Layout>

                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}


SignUp = withFormik({
    mapPropsToValues: () => ({
        name: "",
        phone:"",
        email:"",
        password: "",
        confirmPassword:""
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            name: Yup.string().required(t("required")),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            password: Yup.string()
                .required("Parolni kiriting!")
                .min(8,'8 belgidan kam kiritmang !'),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], 'Passwords must match')
                .required("Parolni kiriting!")
                .min(8,'8 belgidan kam kiritmang !'),
            email: Yup.string()
                .required("Pochta manzilingizni kiriting!")
                .email("Pochta manzili bo'lishi kerak"),
            // phone:Yup.
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);

        let {name, phone,email,password,confirmPassword} = values;
        // let {route} = this.props
        // let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("Props: ", this.props)
        // setSubmitting(true);
        Routines.auth.signUp({
            request: {
                data: {
                    name: name,
                    phone:phone,
                    email:email,
                    type:'personal',
                    password: password,
                    confirmPassword:confirmPassword
                }
            }
        }, props.dispatch)
            .then((data) => {
                if(get(data.response,'data.success',false)){
                    NavigationService.navigate('home')
                    return (
                        Components.Toast.show("Siz muvaffaqiyatli ro'yxatdan o'tdingiz")
                    )

                } else {
                    console.log("Success")
                }
            })
            .catch(e => {
                // setSubmitting(false);
                // if (e.message === "NETWORK_ERROR") {
                // }
            });
    }
})(SignUp);
const mapStateToProps = (state, ownProps) => {
    return {};
};

SignUp = connect(mapStateToProps)(SignUp)
export default withTranslation('main')(SignUp)
