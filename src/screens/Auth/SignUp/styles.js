import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        // backgroundColor:'rgba(255,255,255,0.74)'
        backgroundColor:'#fff'
    },
    wrapper: {
        // flex:1,
        flexGrow:1,
        // paddingVertical: 16
    },
    welcome:{
        marginTop:'11%',
        textAlign:'center',
        fontSize:49,
        marginBottom: 11,
        fontFamily:'Raleway-Bold'
    },
    text:{
        textAlign:'center',
        fontSize:16,
        fontFamily: 'Raleway-Regular',
        color:'rgba(0,0,0,0.6)',
        paddingHorizontal:20,
        marginBottom:'5%'
    },
    component:{
        marginTop: '18%'
    },
    row:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingHorizontal: 24,
        alignItems:'center',
        marginTop:13
    },
    checkLabel:{
        width:'90%',
        fontSize:14,
        fontFamily:'Poppins-Regular',
        color:colors.gray
    },
    error:{
        color:'#e00a13',
        fontSize: 12,
        marginHorizontal:24
    },
})

export default styles
