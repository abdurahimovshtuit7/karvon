import React, {Component} from 'react'
import {
    SafeAreaView,
    Text,
    View,
    TouchableOpacity,
    StatusBar,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
import show from 'components/Toast'
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import {saveUser} from '../../../services/actions';

class Login extends Component {
    state = {
        icon: 'eye-off',
        password: true,
    }
    _changeIcon () {
        this.setState(prevState => ({
            icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
            password: !prevState.password,
        }))
    }
    render() {
        const {handleSubmit,errors,t} = this.props
        // console.log("USER_DATA: ",userData)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}>
                    <Components.Layout style={{flex: 1}}>
                        <Text style={styles.welcome}>
                            Karvon
                        </Text>
                        <Text style={styles.text}>
                            {t('Description')}
                        </Text>
                        <Field
                            name={'phone'}
                            label={'phone number'}
                            title={t('telefon_raqam')}
                            // prefix={"+998"}
                            placeHolder={'(+998) 99 999 99 99'}
                            component={Components.Input}
                            mask={"(+998) [00] [000] [00] [00]"}
                            // formatText={(value) => {
                            //     const onlyNums = value.replace(/[^\d]/g, '');
                            //     if (onlyNums.length <= 2) {
                            //         return `${onlyNums}`
                            //     }
                            //     if (onlyNums.length <= 5) {
                            //         return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2)}`
                            //     }
                            //     if (onlyNums.length <= 7) {
                            //         return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5)}`
                            //     }
                            //     return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5, 7)}-${onlyNums.slice(7)}`
                            // }}
                        />
                        {errors.phone ? (
                            <Text style={styles.error}>{errors.phone}</Text>
                        ) : null}


                        <Field
                            name={'password'}
                            styles={styles.field}
                            label={'password'}
                            title={t('Parol')}
                            secureTextEntry={this.state.password}
                            placeHolder={t('Parol')}
                            component={Components.Input}
                            icon={<Icon
                                name={this.state.icon}
                                style={styles.icon}
                                // onPress={() => this._changeIcon()}
                                size={25}
                                color={'gray'}
                            />}
                        />


                        {errors.password ? (
                            <Text style={styles.error}>{errors.password}</Text>
                        ) : null}
                        {/*<Field*/}
                        {/*    name={"phone"}*/}
                        {/*    prefix={"+998"}*/}
                        {/*    component={Components.TextField}*/}
                        {/*    label={"Номер телефона"}*/}
                        {/*    formatText={(value) => {*/}
                        {/*        const onlyNums = value.replace(/[^\d]/g, '');*/}
                        {/*        if (onlyNums.length <= 2) {*/}
                        {/*            return `${onlyNums}`*/}
                        {/*        }*/}
                        {/*        if (onlyNums.length <= 5) {*/}
                        {/*            return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2)}`*/}
                        {/*        }*/}
                        {/*        if (onlyNums.length <= 7) {*/}
                        {/*            return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5)}`*/}
                        {/*        }*/}
                        {/*        return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5, 7)}-${onlyNums.slice(7)}`*/}
                        {/*    }}*/}
                        {/*/>*/}

                            <Text style={styles.forgot} onPress={()=>{
                                NavigationService.navigate('forgotPassword')
                            }}>
                                {t('ForgotPassword')}
                            </Text>

                        <Components.Submit text={t('kirish')}
                                           color={colors.white}
                                           background = {colors.brandColor}
                                           touch={() => {
                            handleSubmit()
                        }}/>
                        <Components.Submit text={t('Royxatdan_utish')}
                                           color={colors.black}
                                           background = {colors.yellow}
                                           touch={() => {
                                               // handleSubmit()
                                               NavigationService.navigate('selectAuth')
                                           }}/>

                    </Components.Layout>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}

Login = withFormik({
    mapPropsToValues: () => ({
        phone: "",
        password: ""
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            phone: Yup.string()
                .required(t("required"))
                .min(18, 'Belgilar soni kam!'),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),
            password: Yup.string()
                .min(8, 'Belgilar soni kam!')
                .max(16, 'Belgilar soni juda ko\'p!')
                .required(t("required")),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);

        let {phone, password} = values;

        let onlyNums = phone.replace(/[^\d]/g, '');
        console.log("PHONE: ", onlyNums)
        // setSubmitting(true);
        Routines.auth.Login({
            request: {
                data: {
                    phone: '+' + onlyNums,
                    password: password
                }
            }
        }, props.dispatch)
            .then((data) => {
                // console.log("DATA:",get(data,'response',false))
                if(get(data.response,'data.success',false)){
                    NavigationService.navigate('home'),
                    props.dispatch(saveUser(data.response.data))

                }

                // if (data.response.data.token) {
                //     // NavigationService.reset('main', 0)
                //     this.props.dispatch(saveUser(data.response.data))
                // }
                // console.log("Login: ",props.login),
                //     props.dispatch(saveUser(props.login))
                // if (get(props.userData,'data.access_token','')){
                //     // console.log("TOKEN:",get(props.userData,'data.access_token',''),
                //     NavigationService.reset('tab', 0,{
                //         // phone,
                //         // countryCode: code
                //     })}
            })
            .catch(e => {
                // console.log("EX:",e.message)
                // if(!get(e.message,'success',false)){
                //     console.log("FALSE")
                //     return (
                //         Components.Toast.show("Foydalanuvchi topilmadi")
                //     )
                //
                // } else if(e.message === "NETWORK_ERROR"){
                //     return (
                //         Components.Toast.show("Tarmoqqa ulanmagan")
                //     )
                //
                // }
                // setSubmitting(false);
                // if (e.message === "NETWORK_ERROR") {
                // }
            });
    }
})(Login);
const mapStateToProps = (state, ownProps) => {
    return {

    };
};

Login = connect(mapStateToProps)(Login)
export default withTranslation('main')(Login)
