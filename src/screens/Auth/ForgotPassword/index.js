import React, {Component} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";

class ForgotPassword extends Component {
    render() {
        const {handleSubmit,errors,t} = this.props
        // console.log("USER_DATA: ",userData)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}>
                    <Components.Layout style={{flex: 1}}>
                        <Text style={styles.welcome}>
                            Karvon
                        </Text>
                        <Text style={styles.text}>
                            {t('recovery_password')}
                        </Text>
                        <Field
                            name={'phone'}
                            label={'phone number'}
                            title={t('telefon_raqam')}
                            // prefix={"+998"}
                            placeHolder={'(+998) 99 999 99 99'}
                            component={Components.Input}
                            mask={"(+998) [00] [000] [00] [00]"}
                            // formatText={(value) => {
                            //     const onlyNums = value.replace(/[^\d]/g, '');
                            //     if (onlyNums.length <= 2) {
                            //         return `${onlyNums}`
                            //     }
                            //     if (onlyNums.length <= 5) {
                            //         return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2)}`
                            //     }
                            //     if (onlyNums.length <= 7) {
                            //         return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5)}`
                            //     }
                            //     return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5, 7)}-${onlyNums.slice(7)}`
                            // }}
                        />
                        {errors.phone ? (
                            <Text style={styles.error}>{errors.phone}</Text>
                        ) : null}
                        <Field
                            name={'password'}
                            label={'password'}
                            title={t('new_password')}
                            placeHolder={t('new_password')}
                            component={Components.Input}
                        />
                        {errors.password ? (
                            <Text style={styles.error}>{errors.password}</Text>
                        ) : null}
                        {/*<Field*/}
                        {/*    name={"phone"}*/}
                        {/*    prefix={"+998"}*/}
                        {/*    component={Components.TextField}*/}
                        {/*    label={"Номер телефона"}*/}
                        {/*    formatText={(value) => {*/}
                        {/*        const onlyNums = value.replace(/[^\d]/g, '');*/}
                        {/*        if (onlyNums.length <= 2) {*/}
                        {/*            return `${onlyNums}`*/}
                        {/*        }*/}
                        {/*        if (onlyNums.length <= 5) {*/}
                        {/*            return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2)}`*/}
                        {/*        }*/}
                        {/*        if (onlyNums.length <= 7) {*/}
                        {/*            return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5)}`*/}
                        {/*        }*/}
                        {/*        return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5, 7)}-${onlyNums.slice(7)}`*/}
                        {/*    }}*/}
                        {/*/>*/}

                        <Text style={styles.forgot}>

                        </Text>

                        <Components.Submit text={t('reset_password')}
                                           color={colors.white}
                                           background = {colors.brandColor}
                                           touch={() => {
                                               handleSubmit()
                                           }}/>
                    </Components.Layout>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}

ForgotPassword = withFormik({
    mapPropsToValues: () => ({
        phone: "",
        password: ""
    }),
    validationSchema: ({t}) => {
        return Yup.object().shape({
            phone: Yup.string()
                .required(t('required'))
                .min(18, 'Belgilar soni kam!'),
            // .matches(/^[a-z]+^[A-Z]+$/ , 'Is not in correct format'),

            password: Yup.string()
                .min(8, 'Belgilar soni kam!')
                .max(16, 'Belgilar soni juda ko\'p!')
                .required('Maydonni to\'ldiring'),
        });
    },
    handleSubmit: (values, {props, setSubmitting}) => {
        console.log(values);

        let {phone, password} = values;

        let onlyNums = phone.replace(/[^\d]/g, '');
        console.log("PHONE: ", onlyNums)
        // setSubmitting(true);
        Routines.auth.forgotPassword({
            request: {
                data: {
                    phone: '+' + onlyNums,
                }
            }
        }, props.dispatch)
            .then((data) => {
                console.log("PASSWORD: ")
                NavigationService.navigate('smsVerify',{password:password,phone:phone})

                //     props.dispatch(saveUser(props.login))
                // if (get(props.userData,'data.access_token','')){
                //     // console.log("TOKEN:",get(props.userData,'data.access_token',''),
                //     NavigationService.reset('tab', 0,{
                //         // phone,
                //         // countryCode: code
                //     })}
            })
            .catch(e => {
                // setSubmitting(false);
                if (e.message === "NETWORK_ERROR") {
                    Components.Toast.show("Tarmoqni tekshiring!")

                }
            });
    }
})(ForgotPassword);
const mapStateToProps = (state, ownProps) => {
    return {

    };
};

ForgotPassword = connect(mapStateToProps)(ForgotPassword)
export default withTranslation('main')(ForgotPassword)
