import React, {Component, useState} from 'react'
import {
    SafeAreaView,
    Text,
    TouchableOpacity,
    StatusBar, View, TextInput,
} from "react-native";
import {KeyboardAwareScrollView} from "react-native-keyboard-aware-scroll-view";
import {Formik, Field, Form, withFormik} from 'formik';
import * as Yup from 'yup'
import {withTranslation} from "react-i18next";

import styles from './styles'
import colors from "assets/styles/colors";
import Components from 'components'
import {Routines} from "services/api";
import {get} from 'lodash'
import {connect} from "react-redux";
// import {saveUser} from "services/actions";
import NavigationService from "navigators/NavigationService";
import {CodeField, Cursor, useBlurOnFulfill, useClearByFocusCell} from "react-native-confirmation-code-field";


class SmsVerify extends Component {
    state={
        value:'',
        error:false
    }
    editPassword() {
         let {phone, password} = this.props.route.params;
         let onlyNums = phone.replace(/[^\d]/g, '');
        // console.log("PHONE: ", onlyNums)
     console.log("EDITPASSWORD")
    Routines.auth.editPassword({
                                   request: {
                                       data: {
                                           password:password,
                                       },
                                       headers: {
                                           phone: '+' + onlyNums,
                                           code: this.state.value
                                       },
                                   }
                               }, this.props.dispatch)
.then((data) => {
    if(get(this.props.edit,'success',false)){
        NavigationService.navigate("login")
        return (Components.Toast.show(get(data.response,'data.data','')))
    }
})
.catch(e => {
    // setSubmitting(false);
    // if (e.message === "NETWORK_ERROR") {
    // }
});
}
    onChange(text){
        this.setState({value:text});
    }
    render() {
        const {route,edit,t} = this.props
        console.log("EDITED:",get(edit,'success',false))

        console.log("Value: ",this.state.value)
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor={colors.white} barStyle="dark-content" translucent={false}/>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={false}
                                         contentContainerStyle={styles.wrapper}>
                    <Components.Layout style={{flex: 1}}>
                        <Text style={styles.welcome}>
                            {t('confirm')}
                        </Text>
                        <Text style={styles.text}>
                            {t('code')}
                        </Text>
                       <Components.Confirmation  value={this.state.value} setValue={text => this.onChange(text)} getValue={(value)=>{
                           this.setState({value:value})
                       }}/>
                        {this.state.error ? (
                            <Text style={styles.error}>{t('required')}</Text>
                        ) : null}

                        {/*<Field*/}
                        {/*    name={"phone"}*/}
                        {/*    prefix={"+998"}*/}
                        {/*    component={Components.TextField}*/}
                        {/*    label={"Номер телефона"}*/}
                        {/*    formatText={(value) => {*/}
                        {/*        const onlyNums = value.replace(/[^\d]/g, '');*/}
                        {/*        if (onlyNums.length <= 2) {*/}
                        {/*            return `${onlyNums}`*/}
                        {/*        }*/}
                        {/*        if (onlyNums.length <= 5) {*/}
                        {/*            return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2)}`*/}
                        {/*        }*/}
                        {/*        if (onlyNums.length <= 7) {*/}
                        {/*            return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5)}`*/}
                        {/*        }*/}
                        {/*        return `${onlyNums.slice(0, 2)} ${onlyNums.slice(2, 5)}-${onlyNums.slice(5, 7)}-${onlyNums.slice(7)}`*/}
                        {/*    }}*/}
                        {/*/>*/}

                        <Text style={styles.forgot}>

                        </Text>

                        <Components.Submit text={t('Submit')}
                                           color={colors.white}
                                           background = {colors.brandColor}
                                           touch={() => {
                                               (this.state.value.length===4)?(this.editPassword()):(this.setState({error:true}))
                                           }}/>
                    </Components.Layout>
                </KeyboardAwareScrollView>
            </SafeAreaView>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
        edit:state.profile.edit
    };
};
SmsVerify = connect(mapStateToProps)(SmsVerify)
export default withTranslation('main')(SmsVerify)
