import {StyleSheet} from 'react-native';
import colors from 'assets/styles/colors'

const styles = StyleSheet.create({
    container:{
        flex: 1,
        //alignItems: 'center',
        //justifyContent: 'space-between',
        backgroundColor:colors.white

    },
    wrapper: {
        // flex:1,
        flexGrow:1,
        paddingVertical: 16
    },
    welcome:{
        marginTop:'33%',
        textAlign:'center',
        fontSize:22,
        marginBottom: 11,
        fontFamily:'Raleway-Bold'
    },
    text:{
        textAlign:'center',
        fontSize:16,
        fontFamily: 'Raleway-Regular',
        color:'rgba(0,0,0,0.6)',
        paddingHorizontal:50,
        marginBottom:'6%'
    },

    forgot:{
        color:colors.brandColor,
        fontSize:12,
        marginHorizontal:'9%',
        textAlign:'right',
        marginBottom:'10%'
    },
    error:{
        color:'#e00a13',
        fontSize: 12,
        // marginHorizontal:'9%',
        textAlign:'center'
    },
    root: {padding: 20},
    title: {textAlign: 'center', fontSize: 30},
    codeFieldRoot: {
        marginTop: 20,
        width: '60%',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    cellRoot: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#cccccc',
        borderBottomWidth: 1,
    },
    cellText: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center',
    },
    focusCell: {
        borderBottomColor: colors.brandColor,
        borderBottomWidth: 2,
    },

})

export default styles
