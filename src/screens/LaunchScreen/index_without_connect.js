import React, { Component } from 'react'
//import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
    View,
    Text,
    ActivityIndicator,
    Platform
} from 'react-native'
import AnimatedEllipsis from 'react-native-animated-ellipsis';
//import { StackActions } from '@react-navigation/native';
//import { NavigationActions } from '@react-navigation/compat';
//import {Routines} from "../../services/api";
import colors from "../../assets/styles/colors";
import styles from './styles'
import NavigationService from "../../navigators/NavigationService";
import i18next from 'i18next';
import {Api} from '../../services/api';
import {CommonActions} from '@react-navigation/native';

class LaunchScreen extends Component {

    render () {
        // console.warn(JSON.stringify(this.props.login))
        return (
            <View style={styles.container}>
                <Text style={styles.brandName}>
                    Karvon
                </Text>
                <Text style={styles.loading}>
                    Loading
                    {/*<AnimatedEllipsis numberOfDots={3}*/}
                    {/*                  minOpacity={0.4}*/}
                    {/*                  style={{*/}
                    {/*                      color: '#fff',*/}
                    {/*                      fontSize: 10,*/}
                    {/*                      marginTop:20,*/}
                    {/*                      letterSpacing: -5,*/}
                    {/*                  }}/>*/}
                </Text>
                <Text style={styles.footer}>
                    Powered by © UMDSOFT
                </Text>
            </View>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {

    }
}
export default connect(mapStateToProps)(LaunchScreen)
