import {StyleSheet, Platform} from 'react-native';
import colors from 'assets/styles/colors'
import { Dimensions } from 'react-native';

const styles = StyleSheet.create({
   container:{
       flex:1,
       alignItems: 'center',
       // justifyContent: 'center',
       backgroundColor:colors.brandColor
   },
    brandName:{
        fontSize:49,
        color:colors.white,
        marginTop:'80%',
        fontFamily:'Raleway-Bold'
    },
    footer:{
        fontSize: 12,
        color:colors.footer,
        position:'absolute',
        bottom:31,
        fontFamily:'Poppins-Regular'
    },
    loading:{
        fontSize:14,
        color:colors.white,
        marginTop:9,
        fontFamily:'Raleway-Regular'
    }

})
export default styles
