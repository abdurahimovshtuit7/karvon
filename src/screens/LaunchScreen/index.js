import React, {Component} from 'react';
import {connect} from 'react-redux';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import NavigationService from 'navigators/NavigationService';
import Launch from './index_without_connect';
import i18next from 'i18next';
import {CommonActions} from '@react-navigation/native';

import {Platform, StatusBar} from 'react-native';
import colors from 'assets/styles/colors';

class LaunchScreen extends Component {
    getData() {
        setTimeout(() => {
            this.props.navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {name: 'drawer'},
                    ],
                }),
            );
        }, 200);
    }

    componentDidMount() {
        this.getData();
        i18next.changeLanguage(this.props.currentLangCode);
    };

    render() {
        // console.log("Render:",this.props.navigation)
        return (
            <>
                <Launch/>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        currentLangCode: state.language.lang,
    };
};
export default connect(mapStateToProps)(LaunchScreen);
