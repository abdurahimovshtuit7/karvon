import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/lib/integration/react';
import createStore from './src/services/createStore';
import {
  StatusBar,
  View,
  Text
} from 'react-native';

import colors from './src/assets/styles/colors';
import AppNavigators from './src/navigators/AppNavigators';
import LaunchScreen from './src/screens/LaunchScreen/index_without_connect';
import NavigationService from './src/navigators/NavigationService';
import {I18nextProvider} from 'react-i18next';
import i18n from 'services/i18n';
import {NavigationContainer} from '@react-navigation/native';

const {store, persistor} = createStore();

class App extends Component {

  render() {
    return (
        <Provider store={store}>
          <NavigationContainer ref={NavigationService._navigator}>
            <PersistGate loading={<LaunchScreen/>} persistor={persistor}>
              <I18nextProvider i18n={i18n()}>
                <StatusBar
                    backgroundColor={colors.brandColor}
                    barStyle="light-content"
                    translucent={false}
                />
                <AppNavigators/>
              </I18nextProvider>
            </PersistGate>
          </NavigationContainer>
        </Provider>

    );
  }
}

export default App;
